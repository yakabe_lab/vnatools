﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Windows.Forms.DataVisualization.Charting;

namespace Utils
{
    public partial class PowerPlot_Form : Form
    {
        private static readonly int portOffset = 3;
        private static readonly int portCount = 4;

        private readonly FreqRange fr;

        public IProgress<PortPowerStatus> Progress { get; private set; }

        public PowerPlot_Form(FreqRange fr)
        {
            InitializeComponent();

            this.fr = fr;
            Progress = new Progress<PortPowerStatus>(AddData);

            chartInit();
        }

        private void chartInit()
        {
            chart.Series.Clear();
            chart.ChartAreas[0] = new ChartArea();
            chart.ChartAreas[0].AxisX.Title = "周波数 [Hz]";
            chart.ChartAreas[0].AxisX.Maximum = fr.Max / 1e9;
            chart.ChartAreas[0].AxisX.Minimum = fr.Min / 1e9;
            chart.ChartAreas[0].AxisY.Title = "電力 [mW]";
            for (int i = 0; i < portCount; i++)
            {
                chart.Series.Add("P" + (i + portOffset).ToString());
                chart.Series[i].ChartType = SeriesChartType.Line;
            }
        }

        public void AddData(PortPowerStatus status)
        {
            if (status.Freq == fr.Min)
                ClearChart();
            for (int i = 0; i < status.Power.Length; i++)
            {
                chart.Series[i].Points.AddXY(status.Freq / 1e9, status.Power[i]);
            }
            progressBar.Value = status.Percentage;
        }

        public void ClearChart()
        {
            foreach (var s in chart.Series)
                s.Points.Clear();
        }
    }
}
