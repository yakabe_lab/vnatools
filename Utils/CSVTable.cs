﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using Microsoft.VisualBasic.FileIO;
using System.Windows.Forms;

namespace Utils
{
    public class CSVTable
    {
        // --------------------------------------------------

        // 表
        private List<double[]> Table;

        // 表へのアクセス（要素個別）
        public double this[int i, int j]
        {
            set { this.Table[i][j] = value; }
            get { return this.Table[i][j]; }
        }

        // 表へのアクセス（行別）
        public double[] this[int i]
        {
            set { this.Table[i] = value; }
            get { return this.Table[i]; }
        }

        // --------------------------------------------------

        /// <summary>
        /// CSVファイルのコメント行 
        /// </summary>
        public String Comment { get; set; }

        // --------------------------------------------------
        //              ここから先は公開メソッド
        // --------------------------------------------------

        public void test()
        {
            Console.WriteLine(string.Join(",",
                Array.ConvertAll<double, string>(Table[0], s => s.ToString())));
        }

        // 行要素の追加
        public void Add(double[] val)
        {
            Table.Add(val);
        }

        /// <summary>
        /// 空欄リストのコンストラクタ
        /// </summary>
        public CSVTable()
        {
            Table = new List<double[]>();
            Comment = "";
        }

        /// <summary>
        /// CSVファイルを使ったコンストラクタ
        /// </summary>
        /// <param name="FileName">ファイル名フルパス</param>
        public CSVTable(String FileName)
            : this()    // 無名コンストラクタの呼び出し
        {
            ReadCSV(FileName);
        }

        /// <summary>
        /// 現在保持されている行数を返す
        /// </summary>
        /// <returns></returns>
        public int RowCount()
        {
            return Table.Count;
        }

        /// <summary>
        /// 1行目に保持されている行要素の列数を返す
        /// </summary>
        /// <returns></returns>
        public int ColCount()
        {
            return Table[0].Count();
        }

        /// <summary>
        /// CSVファイルからテーブルを作成する
        /// </summary>
        /// <param name="FileName">ファイル名フルパス</param>
        public void ReadCSV(String FileName)
        {
            TextFieldParser parser = new TextFieldParser(FileName,
                System.Text.Encoding.GetEncoding("Shift_JIS"));

            using (parser)
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(new String[] { ",", " " }); // 区切り文字はコンマかスペース
                parser.CommentTokens = new string[] { "#", "!" };

                //parser.HasFieldsEnclosedInQuotes = false;
                parser.TrimWhiteSpace = false;

                while (!parser.EndOfData)
                {
                    string[] row = parser.ReadFields(); // 1行読み込み
                    Table.Add(Array.ConvertAll<string, double>(row, s => double.Parse(s)));
                }

            }
        }

        /// <summary>
        /// 指定ファイルからファイル上部のコメント行のみを読み込む
        /// </summary>
        /// <param name="FileName"></param>
        public void ReadComment(string FileName)
        {

            StreamReader sr =
                new StreamReader(FileName, Encoding.GetEncoding("Shift_JIS"));

            using (sr)
            {
                StringBuilder sb = new StringBuilder();
                string line;

                while ((line = sr.ReadLine()) != null)
                {
                    // コメント行が続く限り読み込み続ける
                    if ((line != "") && ((line[0] == '#') || (line[0] == '!')))
                        sb.AppendLine(line);
                    else
                        break;
                }
                Comment = sb.ToString();
            }
        }

        /// <summary>
        /// CSVファイルを指定ファイル名で保存する
        /// </summary>
        /// <param name="FileName">ファイル名フルパス</param>
        public void WriteCSV(String FileName)
        {
            StreamWriter sw =
                new StreamWriter(FileName, false, Encoding.GetEncoding("Shift_JIS"));

            using (sw)
            {
                // コメントの書き込み
                if (Comment != "")
                    sw.Write(Comment);

                // 要素の書き込み
                foreach (double[] row in Table)
                {
                    sw.WriteLine(string.Join(",",
                        Array.ConvertAll<double, string>(row, s => s.ToString())));
                }
            }
        }
    }
}
