﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Instruments
{
    public class DeviceSetting
    {
        public string Name { get; private set; }
        public int PortNum { get; private set; }

        public DeviceSetting(string name, int portNum)
        {
            Name = name;
            PortNum = portNum;
        }
    }
}
