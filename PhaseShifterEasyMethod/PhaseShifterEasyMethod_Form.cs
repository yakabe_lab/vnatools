﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Threading;
using Utils;
using Gpib;
using Instruments;
using System.IO;

namespace PhaseShifterEasyMethod
{
    public partial class PhaseShifterEasyMethod_Form : Form
    {
        private Dictionary<int, string> gpibDevices = new Dictionary<int, string>();
        private List<ConnectInfo> connectInfos = new List<ConnectInfo>();
        private CancellationTokenSource cts;
        private List<Control> allControls;

        private Dictionary<InstType, ComboBox> deviceCbDict;
        private Dictionary<InstType, ComboBox> portCbDict;

        public PhaseShifterEasyMethod_Form()
        {
            InitializeComponent();

            allControls = getAllControls<Control>(this);
            button_Cancel.Enabled = false;

            deviceCbDict = new Dictionary<InstType, ComboBox>()
            {
                {InstType.Osc, comboBox_Osc}, {InstType.Pm, comboBox_Pm},
                {InstType.Ts, comboBox_Ts}, {InstType.Ps, comboBox_Ps},
            };
            portCbDict = new Dictionary<InstType, ComboBox>()
            {
                {InstType.Osc, comboBox_OscPort}, {InstType.Pm, comboBox_PmPort},
                {InstType.Ts, comboBox_TsPort}, {InstType.Ps, comboBox_PsPort},
            };

            foreach (var pair in deviceCbDict)
            {
                foreach (var inst in InstDefs.InstDicts[pair.Key])
                    pair.Value.Items.Add(inst.Key);
                pair.Value.SelectedIndex = pair.Value.Items.Count - 1;
            }

            // GPIBとシリアルポート取得
            button_Reflesh_Click(this, EventArgs.Empty);
        }

        private void button_SaveFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.Description = "保存フォルダの選択";
            if (fbd.ShowDialog(this) == DialogResult.OK)
            {
                textBox_SaveFolder.Text = fbd.SelectedPath;
                if (!Misc.IsEmptyDirectory(fbd.SelectedPath))
                {
                    DialogResult res = MessageBox.Show(
                        "空フォルダではありませんがよろしいですか", "上書き確認",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk);
                    if (res == DialogResult.No)
                        button_SaveFolder_Click(sender, e);
                }
            }
        }

        private async void button_StartCAL_Click(object sender, EventArgs e)
        {
            var fr = new FreqRange(
                (long)numericUpDown_StartFreq.Value * 1000000,
                (long)numericUpDown_StopFreq.Value * 1000000,
                int.Parse(textBox_FreqPoints.Text));
            allControls.ForEach(c => c.Enabled = false);
            button_Cancel.Enabled = true;

            var deviceSettings = new Dictionary<InstType, DeviceSetting>();
            foreach (var type in deviceCbDict.Keys)
            {
                deviceSettings[type] =
                    new DeviceSetting(deviceCbDict[type].Text,
                        connectInfos[portCbDict[type].SelectedIndex].PortNum);
            }
            
            cts = new CancellationTokenSource();
            var measure = new EasyMethodMeasure(deviceSettings, fr, double.Parse(textBox_OscPower.Text),
                int.Parse(textBox_WaitTime.Text), int.Parse(comboBox_RefPort.Text),　textBox_SaveFolder.Text, cts.Token);
            await measure.Measure();
            allControls.ForEach(c => c.Enabled = true);
            button_Cancel.Enabled = false;
        }

        // 接続機器取得
        private void button_Reflesh_Click(object sender, EventArgs e)
        {
            connectInfos.Clear();
            connectInfos.Add(new ConnectInfo(ConnectionType.None, 0, "Dummy"));

            ContecGpib gpib = null;
            try
            {
                gpib = new ContecGpib("GPIB000");
                gpibDevices = gpib.Search(); // GPIBデバイス検索
                // GPIBデバイス追加
                foreach (var d in gpibDevices)
                    connectInfos.Add(new ConnectInfo(ConnectionType.Gpib, d.Key, d.Value));
            }
            catch (Exception) { }
            finally
            {
                if (gpib != null)
                    gpib.Close();
            }

            // シリアルデバイス検索
            string[] serialDevices = Misc.GetDeviceNames();
            // シリアルデバイス追加
            if (serialDevices != null)
            {
                foreach (string s in serialDevices)
                    connectInfos.Add(new ConnectInfo(
                        ConnectionType.Serial, Misc.ExtractComNum(s), "Serial Device"));
            }

            foreach (var pair in portCbDict)
            {
                pair.Value.Items.Clear();
                foreach (var info in connectInfos)
                    pair.Value.Items.Add(info);
                pair.Value.SelectedIndex = 0;
            }
            // GPIB機器自動選択
            searchGpibPort();
        }

        private void searchGpibPort()
        {
            if (gpibDevices == null) return;

            foreach (var it in gpibDevices)
            {
                if (it.Value.IndexOf((string)comboBox_Osc.SelectedItem) != -1)
                {
                    comboBox_OscPort.SelectedItem = it;
                    break;
                }
            }
            foreach (var it in gpibDevices)
            {
                if (it.Value.IndexOf((string)comboBox_Pm.SelectedItem) != -1)
                {
                    comboBox_PmPort.SelectedItem = it;
                    break;
                }
            }
        }
        
        private void button_Cancel_Click(object sender, EventArgs e)
        {
            cts.Cancel();
            allControls.ForEach(c => c.Enabled = true);
            button_Cancel.Enabled = false;
        }

        /// <summary>
        /// 指定のコントロール上の全てのジェネリック型 Tコントロールを取得する。
        /// </summary>
        /// <typeparam name="T">対象となる型</typeparam>
        /// <param name="top">指定のコントロール</param>
        /// <returns>指定のコントロール上の全てのジェネリック型 Tコントロールのインスタンス</returns>
        private static List<T> getAllControls<T>(Control top) where T : Control
        {
            List<T> buf = new List<T>();
            foreach (Control ctrl in top.Controls)
            {
                if (ctrl is T) buf.Add((T)ctrl);
                buf.AddRange(getAllControls<T>(ctrl));
            }
            return buf;
        }
    }
}
