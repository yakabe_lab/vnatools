﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Gpib;

namespace Instruments.Oscillator
{
    public class Osc_E8257D : IOscillator
    {
        ContecGpib gpib;
        int addr; // 物理アドレス

        public Osc_E8257D(ContecGpib gpib, int addr)
        {
            this.gpib = gpib;
            this.addr = addr;
            gpib.Open(addr);

            // 初期化
            gpib.Send(addr, "*RST");
            gpib.Send(addr, "*CLS");
        }

        public void Dispose() { }

        /// <summary>
        /// RF出力ON/OFFの設定
        /// </summary>
        /// <param name="on"></param>
        public void SetOutput(bool on)
        {
            if (on)
            {
                gpib.Send(addr, ":OUTP ON");
            }
            else
            {
                gpib.Send(addr, ":OUTP OFF");
            }
        }

        /// <summary>
        /// 出力電力の設定
        /// </summary>
        /// <param name="dBm"></param>
        public void SetPower(double dBm)
        {
            gpib.Send(addr, ":POW:AMPL " + dBm.ToString("0.00") + "dBm");
        }

        /// <summary>
        /// 周波数設定(SetFrequencyRangeで指定した最小の周波数を設定するとスイープ・データ取得開始)
        /// </summary>
        /// <param name="freq"></param>
        public void SetFreq(long freq)
        {
            gpib.Send(addr, ":FREQ " + freq.ToString() + "Hz");
        }

    }
}
