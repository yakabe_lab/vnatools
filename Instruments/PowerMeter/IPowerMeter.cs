﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Instruments.PowerMeter
{
    public interface IPowerMeter : IDisposable, IInstrument
    {
        int PortCount { get; }
        double[] GetPower(long freq, string msg);
    }
}
