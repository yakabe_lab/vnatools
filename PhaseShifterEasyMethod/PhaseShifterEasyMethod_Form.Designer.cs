﻿namespace PhaseShifterEasyMethod
{
    partial class PhaseShifterEasyMethod_Form
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.button_Cancel = new System.Windows.Forms.Button();
            this.comboBox_Ps = new System.Windows.Forms.ComboBox();
            this.button_Reflesh = new System.Windows.Forms.Button();
            this.button_StartCAL = new System.Windows.Forms.Button();
            this.comboBox_PsPort = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.comboBox_TsPort = new System.Windows.Forms.ComboBox();
            this.comboBox_Ts = new System.Windows.Forms.ComboBox();
            this.comboBox_PmPort = new System.Windows.Forms.ComboBox();
            this.comboBox_OscPort = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.comboBox_Pm = new System.Windows.Forms.ComboBox();
            this.comboBox_Osc = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox_WaitTime = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox_OscPower = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_FreqPoints = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.numericUpDown_StopFreq = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_StartFreq = new System.Windows.Forms.NumericUpDown();
            this.button_SaveFolder = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_SaveFolder = new System.Windows.Forms.TextBox();
            this.comboBox_RefPort = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_StopFreq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_StartFreq)).BeginInit();
            this.SuspendLayout();
            // 
            // button_Cancel
            // 
            this.button_Cancel.Location = new System.Drawing.Point(255, 363);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.button_Cancel.TabIndex = 90;
            this.button_Cancel.Text = "中止";
            this.button_Cancel.UseVisualStyleBackColor = true;
            this.button_Cancel.Click += new System.EventHandler(this.button_Cancel_Click);
            // 
            // comboBox_Ps
            // 
            this.comboBox_Ps.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Ps.FormattingEnabled = true;
            this.comboBox_Ps.Location = new System.Drawing.Point(18, 241);
            this.comboBox_Ps.Name = "comboBox_Ps";
            this.comboBox_Ps.Size = new System.Drawing.Size(194, 20);
            this.comboBox_Ps.TabIndex = 77;
            // 
            // button_Reflesh
            // 
            this.button_Reflesh.Location = new System.Drawing.Point(19, 269);
            this.button_Reflesh.Name = "button_Reflesh";
            this.button_Reflesh.Size = new System.Drawing.Size(91, 23);
            this.button_Reflesh.TabIndex = 80;
            this.button_Reflesh.Text = "機器情報取得";
            this.button_Reflesh.UseVisualStyleBackColor = true;
            this.button_Reflesh.Click += new System.EventHandler(this.button_Reflesh_Click);
            // 
            // button_StartCAL
            // 
            this.button_StartCAL.Location = new System.Drawing.Point(109, 363);
            this.button_StartCAL.Name = "button_StartCAL";
            this.button_StartCAL.Size = new System.Drawing.Size(75, 23);
            this.button_StartCAL.TabIndex = 85;
            this.button_StartCAL.Text = "測定開始";
            this.button_StartCAL.UseVisualStyleBackColor = true;
            this.button_StartCAL.Click += new System.EventHandler(this.button_StartCAL_Click);
            // 
            // comboBox_PsPort
            // 
            this.comboBox_PsPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_PsPort.FormattingEnabled = true;
            this.comboBox_PsPort.Location = new System.Drawing.Point(221, 241);
            this.comboBox_PsPort.Name = "comboBox_PsPort";
            this.comboBox_PsPort.Size = new System.Drawing.Size(237, 20);
            this.comboBox_PsPort.TabIndex = 78;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(17, 226);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(66, 12);
            this.label11.TabIndex = 89;
            this.label11.Text = "フェーズシフタ";
            // 
            // comboBox_TsPort
            // 
            this.comboBox_TsPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_TsPort.FormattingEnabled = true;
            this.comboBox_TsPort.Location = new System.Drawing.Point(221, 203);
            this.comboBox_TsPort.Name = "comboBox_TsPort";
            this.comboBox_TsPort.Size = new System.Drawing.Size(237, 20);
            this.comboBox_TsPort.TabIndex = 74;
            // 
            // comboBox_Ts
            // 
            this.comboBox_Ts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Ts.FormattingEnabled = true;
            this.comboBox_Ts.Location = new System.Drawing.Point(19, 203);
            this.comboBox_Ts.Name = "comboBox_Ts";
            this.comboBox_Ts.Size = new System.Drawing.Size(193, 20);
            this.comboBox_Ts.TabIndex = 73;
            // 
            // comboBox_PmPort
            // 
            this.comboBox_PmPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_PmPort.FormattingEnabled = true;
            this.comboBox_PmPort.Location = new System.Drawing.Point(221, 165);
            this.comboBox_PmPort.Name = "comboBox_PmPort";
            this.comboBox_PmPort.Size = new System.Drawing.Size(237, 20);
            this.comboBox_PmPort.TabIndex = 72;
            // 
            // comboBox_OscPort
            // 
            this.comboBox_OscPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_OscPort.FormattingEnabled = true;
            this.comboBox_OscPort.Location = new System.Drawing.Point(221, 127);
            this.comboBox_OscPort.Name = "comboBox_OscPort";
            this.comboBox_OscPort.Size = new System.Drawing.Size(237, 20);
            this.comboBox_OscPort.TabIndex = 68;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(16, 188);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(56, 12);
            this.label13.TabIndex = 88;
            this.label13.Text = "テストセット";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(234, 112);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(33, 12);
            this.label12.TabIndex = 87;
            this.label12.Text = "ポート";
            // 
            // comboBox_Pm
            // 
            this.comboBox_Pm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Pm.FormattingEnabled = true;
            this.comboBox_Pm.Location = new System.Drawing.Point(18, 165);
            this.comboBox_Pm.Name = "comboBox_Pm";
            this.comboBox_Pm.Size = new System.Drawing.Size(194, 20);
            this.comboBox_Pm.TabIndex = 69;
            // 
            // comboBox_Osc
            // 
            this.comboBox_Osc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Osc.FormattingEnabled = true;
            this.comboBox_Osc.Location = new System.Drawing.Point(18, 127);
            this.comboBox_Osc.Name = "comboBox_Osc";
            this.comboBox_Osc.Size = new System.Drawing.Size(194, 20);
            this.comboBox_Osc.TabIndex = 67;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 150);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 12);
            this.label10.TabIndex = 86;
            this.label10.Text = "電力計";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 112);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 12);
            this.label8.TabIndex = 84;
            this.label8.Text = "発振器";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(107, 26);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(17, 12);
            this.label9.TabIndex = 83;
            this.label9.Text = "～";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(354, 62);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(20, 12);
            this.label7.TabIndex = 79;
            this.label7.Text = "ms";
            // 
            // textBox_WaitTime
            // 
            this.textBox_WaitTime.Location = new System.Drawing.Point(305, 59);
            this.textBox_WaitTime.Name = "textBox_WaitTime";
            this.textBox_WaitTime.Size = new System.Drawing.Size(40, 19);
            this.textBox_WaitTime.TabIndex = 65;
            this.textBox_WaitTime.Text = "50";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(177, 62);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(122, 12);
            this.label6.TabIndex = 76;
            this.label6.Text = "周波数変更後待ち時間";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(133, 62);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(28, 12);
            this.label5.TabIndex = 75;
            this.label5.Text = "dBm";
            // 
            // textBox_OscPower
            // 
            this.textBox_OscPower.Location = new System.Drawing.Point(87, 59);
            this.textBox_OscPower.Name = "textBox_OscPower";
            this.textBox_OscPower.Size = new System.Drawing.Size(40, 19);
            this.textBox_OscPower.TabIndex = 64;
            this.textBox_OscPower.Text = "14";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 71;
            this.label4.Text = "発振器電力";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(264, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(17, 12);
            this.label3.TabIndex = 70;
            this.label3.Text = "点";
            // 
            // textBox_FreqPoints
            // 
            this.textBox_FreqPoints.Location = new System.Drawing.Point(218, 23);
            this.textBox_FreqPoints.Name = "textBox_FreqPoints";
            this.textBox_FreqPoints.Size = new System.Drawing.Size(40, 19);
            this.textBox_FreqPoints.TabIndex = 63;
            this.textBox_FreqPoints.Text = "401";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(107, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 66;
            this.label2.Text = "測定周波数";
            // 
            // numericUpDown_StopFreq
            // 
            this.numericUpDown_StopFreq.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numericUpDown_StopFreq.Location = new System.Drawing.Point(132, 24);
            this.numericUpDown_StopFreq.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.numericUpDown_StopFreq.Minimum = new decimal(new int[] {
            8000,
            0,
            0,
            0});
            this.numericUpDown_StopFreq.Name = "numericUpDown_StopFreq";
            this.numericUpDown_StopFreq.Size = new System.Drawing.Size(80, 19);
            this.numericUpDown_StopFreq.TabIndex = 62;
            this.numericUpDown_StopFreq.ThousandsSeparator = true;
            this.numericUpDown_StopFreq.Value = new decimal(new int[] {
            12000,
            0,
            0,
            0});
            // 
            // numericUpDown_StartFreq
            // 
            this.numericUpDown_StartFreq.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numericUpDown_StartFreq.Location = new System.Drawing.Point(18, 24);
            this.numericUpDown_StartFreq.Maximum = new decimal(new int[] {
            12000,
            0,
            0,
            0});
            this.numericUpDown_StartFreq.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDown_StartFreq.Name = "numericUpDown_StartFreq";
            this.numericUpDown_StartFreq.Size = new System.Drawing.Size(80, 19);
            this.numericUpDown_StartFreq.TabIndex = 60;
            this.numericUpDown_StartFreq.ThousandsSeparator = true;
            this.numericUpDown_StartFreq.Value = new decimal(new int[] {
            8000,
            0,
            0,
            0});
            // 
            // button_SaveFolder
            // 
            this.button_SaveFolder.Location = new System.Drawing.Point(383, 323);
            this.button_SaveFolder.Name = "button_SaveFolder";
            this.button_SaveFolder.Size = new System.Drawing.Size(75, 23);
            this.button_SaveFolder.TabIndex = 82;
            this.button_SaveFolder.Text = "参照";
            this.button_SaveFolder.UseVisualStyleBackColor = true;
            this.button_SaveFolder.Click += new System.EventHandler(this.button_SaveFolder_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 308);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 12);
            this.label1.TabIndex = 61;
            this.label1.Text = "保存ディレクトリ";
            // 
            // textBox_SaveFolder
            // 
            this.textBox_SaveFolder.Location = new System.Drawing.Point(18, 325);
            this.textBox_SaveFolder.Name = "textBox_SaveFolder";
            this.textBox_SaveFolder.Size = new System.Drawing.Size(359, 19);
            this.textBox_SaveFolder.TabIndex = 81;
            // 
            // comboBox_RefPort
            // 
            this.comboBox_RefPort.FormattingEnabled = true;
            this.comboBox_RefPort.Items.AddRange(new object[] {
            "3",
            "4",
            "5",
            "6"});
            this.comboBox_RefPort.Location = new System.Drawing.Point(404, 23);
            this.comboBox_RefPort.Name = "comboBox_RefPort";
            this.comboBox_RefPort.Size = new System.Drawing.Size(44, 20);
            this.comboBox_RefPort.TabIndex = 91;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(317, 26);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(81, 12);
            this.label14.TabIndex = 92;
            this.label14.Text = "正規化用ポート";
            // 
            // PhaseShifterEasyMethod_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(474, 395);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.comboBox_RefPort);
            this.Controls.Add(this.button_Cancel);
            this.Controls.Add(this.comboBox_Ps);
            this.Controls.Add(this.button_Reflesh);
            this.Controls.Add(this.button_StartCAL);
            this.Controls.Add(this.comboBox_PsPort);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.comboBox_TsPort);
            this.Controls.Add(this.comboBox_Ts);
            this.Controls.Add(this.comboBox_PmPort);
            this.Controls.Add(this.comboBox_OscPort);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.comboBox_Pm);
            this.Controls.Add(this.comboBox_Osc);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textBox_WaitTime);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBox_OscPower);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox_FreqPoints);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.numericUpDown_StopFreq);
            this.Controls.Add(this.numericUpDown_StartFreq);
            this.Controls.Add(this.button_SaveFolder);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox_SaveFolder);
            this.Name = "PhaseShifterEasyMethod_Form";
            this.Text = "移相器簡易法";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_StopFreq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_StartFreq)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_Cancel;
        private System.Windows.Forms.ComboBox comboBox_Ps;
        private System.Windows.Forms.Button button_Reflesh;
        private System.Windows.Forms.Button button_StartCAL;
        private System.Windows.Forms.ComboBox comboBox_PsPort;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox comboBox_TsPort;
        private System.Windows.Forms.ComboBox comboBox_Ts;
        private System.Windows.Forms.ComboBox comboBox_PmPort;
        private System.Windows.Forms.ComboBox comboBox_OscPort;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox comboBox_Pm;
        private System.Windows.Forms.ComboBox comboBox_Osc;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox_WaitTime;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox_OscPower;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox_FreqPoints;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numericUpDown_StopFreq;
        private System.Windows.Forms.NumericUpDown numericUpDown_StartFreq;
        private System.Windows.Forms.Button button_SaveFolder;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_SaveFolder;
        private System.Windows.Forms.ComboBox comboBox_RefPort;
        private System.Windows.Forms.Label label14;

    }
}

