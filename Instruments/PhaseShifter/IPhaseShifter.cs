﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Instruments.PhaseShifter
{
    public interface IPhaseShifter : IDisposable, IInstrument
    {
        double Delay { get; }
        double SetDelayDifference(double delay);
        double SetDelay(double delay);
        void SetAbsoluteStep(int step);
        void SetRelativeStep(int step);
        void Rotate(int step, Direction d);
    }
}
