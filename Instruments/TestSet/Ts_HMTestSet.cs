﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO.Ports;
using System.Diagnostics;
using System.Windows.Forms;

namespace Instruments.TestSet
{
    public class Ts_HMTestSet : ITestSet
    {
        private SerialPort com;
        public static Dictionary<SwitchState, string> stateStr
            = new Dictionary<SwitchState, string>()
            {
                {SwitchState.CAL, "!CAL"}, {SwitchState.TBL, "!TBL"}, {SwitchState.Ref, "!CAL"}
            };

        /// <summary>
        /// 接続確認時に照合する名前
        /// </summary>
        private const string ModuleName = "sw_controller";


        public Ts_HMTestSet(int port)
        {
            com = new SerialPort();
            com.BaudRate = 9600;
            com.NewLine = "\r\n";

            com.DataBits = 8;
            com.DiscardNull = false;
            com.Encoding = Encoding.ASCII;
            com.Handshake = Handshake.None;
            com.Parity = Parity.None;
            com.StopBits = StopBits.One;

            com.ReadTimeout = 2000;
            com.WriteTimeout = 2000;

            com.PortName = "COM" + port;
            com.Open();
            if (!checkConnection())
                throw new InvalidOperationException(
                    string.Format("テストセットが {0} に接続されていません", com.PortName));
        }

        /// <summary>
        /// 接続の確認をする
        /// </summary>
        /// <returns></returns>
        private bool checkConnection()
        {
            string res = "";

            com.DiscardInBuffer();
            com.DiscardOutBuffer();

            try
            {
                // 「誰？」コマンドを送る
                com.WriteLine("*");
                res = com.ReadLine();
            }
            catch (TimeoutException)
            {
                Close();
                return false;
            }

            if (String.Compare(res, 0, ModuleName, 0, ModuleName.Length) == 0)
            {
                return true;
            }

            // 失敗
            Close();
            return false;
        }

        /// <summary>
        /// 閉じる
        /// </summary>
        public void Close()
        {
            if (com.IsOpen)
            {
                com.Close();
                Debug.WriteLine("TestSet: close");
            }
        }

        public void Dispose()
        {
            Close();
        }

        /// <summary>
        /// テストセット切り替え
        /// </summary>
        /// <param name="set"></param>
        public void SetState(SwitchState s, int output = 0, int input = 0)
        {
            com.DiscardInBuffer();
            com.DiscardOutBuffer();

            if (s == SwitchState.CalRef)
            {
                // 校正用Matchを手動で接続変更
                MessageBox.Show("Matched load を同定用ポートに接続してください", "テストセット",
                    MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }
            else
            {
                string buf = (s == SwitchState.DUT) ?
                    string.Format("!{0}{1}", output, input) : stateStr[s];
                com.WriteLine(buf);
                com.ReadLine();
            }
        }
    }
}
