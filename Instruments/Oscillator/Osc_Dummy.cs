﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Gpib;

namespace Instruments.Oscillator
{
    public class Osc_Dummy : IOscillator
    {
        public Osc_Dummy() { }

        /// <summary>
        /// SG出力のON, OFF
        /// </summary>
        /// <param name="on"></param>
        public void SetOutput(bool on) { }

        /// <summary>
        /// SG出力電力の設定
        /// </summary>
        /// <param name="dBm"></param>
        public void SetPower(double dBm) { }

        /// <summary>
        /// SG出力周波数設定
        /// </summary>
        /// <param name="freq"></param>
        public void SetFreq(long freq) { }

        public void Dispose() { }
    }
}
