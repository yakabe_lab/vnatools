﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;
using System.Threading;
using Utils;

namespace Instruments.Sweeper
{
    public class Swp_FromFile : IInstrument, ISweeper
    {
        private FreqRange fr;
        private long freq;
        private PortPowerFile ppf;

        // 周波数からインデックスを逆引き
        private Dictionary<long, int> indexDictionary = new Dictionary<long,int>();

        public int PortCount { get; private set; }
        
        public void SetFrequencyRange(FreqRange fr)
        {
            this.fr = fr;
            for (int f = 0; f < fr.Count; f++)
                indexDictionary[fr[f]] = f;
        }

        public void SetFreq(long freq)
        {
            this.freq = freq;
        }

        public void SetOutput(bool on) { }

        public void SetPower(double power) { }

        public double[] GetPower(long freq, string msg = "ポート電力ファイルの選択")
        {
            if (fr.Min == freq)
                getPowerFile(msg);

            return ppf[indexDictionary[freq]];
        }

        private void getPowerFile(string msg)
        {
            var ofd = new OpenFileDialog();
            ofd.Title = msg;
            ofd.Filter = "CSVファイル(*.csv)|*.csv|すべてのファイル|*.*";
            DialogResult result = DialogResult.No;

            // STAThreadでないとダイアログを呼べないのでThreadを使用
            var t = new Thread(() => result = ofd.ShowDialog());
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
            t.Join();
            if (result == DialogResult.OK)
            {
                ppf = new PortPowerFile(ofd.FileName);
                PortCount = ppf[0].Length;
                if (ppf.FreqRange != fr)
                {
                    MessageBox.Show("エラー", "周波数範囲が違います",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    getPowerFile(msg);
                }
            }
            else
                getPowerFile(msg);
        }

        public void Dispose() { }
    }
}
