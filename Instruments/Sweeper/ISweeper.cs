﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Instruments.Oscillator;
using Instruments.PowerMeter;
using Utils;

namespace Instruments.Sweeper
{
    /// <summary>
    /// スイープ付き発振器＋電力計
    /// 予め周波数範囲を設定しておき，最低周波数が指定されたらスイープ測定
    /// GetPowers() で指定周波数の測定した電力を返す
    /// </summary>
    public interface ISweeper : IOscillator, IPowerMeter
    {
        void SetFrequencyRange(FreqRange fr);
    }
}
