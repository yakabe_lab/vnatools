﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Numerics;
using System.IO;

namespace Utils
{
    public class SysPara
    {
        public double[] T { get; private set; } // 実数シスパラ
        public Complex[] t { get; private set; } // 複素シスパラ

        public SysPara()
        {
            T = new double[3];
            t = new Complex[4];
        }

        public SysPara(double[] T, Complex[] t)
        {
            if (T.Length != 3 && t.Length != 4)
                throw new Exception("6ポートシステムのシステムパラメータではありません");
            this.T = T;
            this.t = t;
        }
    }

    public class SysParaFile
    {
        private SysPara[] data;
        public FreqRange FreqRange { get; private set; }
        public int RefPort { get; private set; } // 正規化用ポート P3:0, P4:1, ...

        private static string errMsg = "読み込みエラー{0}行目" + Environment.NewLine
            + "{1}" + Environment.NewLine + "{2}";

        public SysPara this[int freqIndex]
        {
            get { return data[freqIndex]; }
        }

        public SysParaFile(SysPara[] data, FreqRange fr, int refPort)
        {
            this.data = data;
            FreqRange = fr;
            RefPort = refPort;
        }

        public SysParaFile(string fileName)
        {
            var freqList = new List<long>();
            var tempData = new List<SysPara>();

            var lines = File.ReadAllLines(fileName);
            for (int l = 0; l < lines.Length; l++)
            {
                if (lines[l].StartsWith("# RefPort: P"))
                {
                    int temp;
                    bool b = int.TryParse(lines[l].Substring(lines[l].Length - 1, 1), out temp);
                    if (b && temp >= 3 && temp <= 6)
                        throw new Exception(string.Format(
                            errMsg, l, lines[l], "不正な正規化ポート番号です"));
                    RefPort = temp - 3;
                }
                if (lines[l].StartsWith("#"))
                    continue;

                string[] values = lines[l].Split(',');
                long freq;
                if (!long.TryParse(values[0], out freq))
                    throw new Exception(string.Format(errMsg, lines[l], l, "周波数がおかしい"));
                freqList.Add(freq);

                tempData.Add(parseLine(values, l));
            }
            data = tempData.ToArray();
        }

        private static SysPara parseLine(string[] values, int lineNum)
        {
            var valueList = new List<double>();
            foreach (var val in values.Skip(1)) // 周波数は飛ばす
            {
                double sysPara;
                if (!double.TryParse(val, out sysPara))
                    throw new Exception(string.Format(
                        errMsg, lineNum, values, "システムパラメータ読み込みエラー"));
                valueList.Add(sysPara);
            }
            if (valueList.Count != 11)
                throw new Exception(string.Format(
                    errMsg, lineNum, values, "システムパラメータ読み込みエラー"));

            var T = valueList.Take(3);
            var t = new List<Complex>();
            for (int i = 3; i < 11; i += 2)
                t.Add(Misc.MagDegToComplex(valueList[i], valueList[i + 1]));

            return new SysPara(T.ToArray(), t.ToArray());
        }

        public void Save(string fileName)
        {
            string[] realHeaders = Enumerable.Range(0, 4).Where(i => i != RefPort)
                .Select(i => (RefPort + 3) + "T" + (i + 3)).ToArray();
            string[] complexHeaders = Enumerable.Range(3, 4)
                .Select(i => string.Format("t{0}(Re), t{0}(Im)", i)).ToArray();
            string comment =
                "# " + DateTime.Now.ToString() + Environment.NewLine +
                "# RefPort: P" + (RefPort + 3) + Environment.NewLine +
                "# Freq[Hz], " + string.Join(", ", realHeaders, complexHeaders) + Environment.NewLine;

            using (var sw = new StreamWriter(fileName))
            {
                sw.Write(comment);
                for (int f = 0; f < FreqRange.Count; f++)
                {
                    sw.Write(FreqRange[f] + "," + string.Join(",", data[f].T));
                    sw.Write(string.Join(",", data[f].t.Select(t => t.Real + "," + t.Imaginary)));
                    sw.Write(Environment.NewLine);
                }
            }
        }
    }
}
