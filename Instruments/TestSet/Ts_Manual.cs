﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;

namespace Instruments.TestSet
{
    public class Ts_Manual : ITestSet
    {
        public Ts_Manual() { }

        public void Dispose() { }

        /// <summary>
        /// テストセット切り替え
        /// </summary>
        /// <param name="set"></param>
        public void SetState(SwitchState s, int output = 0, int input = 0)
        {
            System.Media.SystemSounds.Asterisk.Play();
            MessageBox.Show("テストセットを" + s + "に変更してください。");
        }
    }
}
