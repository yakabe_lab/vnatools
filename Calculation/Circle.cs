﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Numerics;

namespace Calculation
{
    internal class Circle
    {
        internal double Radius { get; private set; }
        internal Complex Center { get; private set; }

        internal Circle(Complex center, double radius)
        {
            Center = center;
            Radius = Radius;
        }
    }
}
