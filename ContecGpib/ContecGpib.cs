﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;
using CgpibCs;

namespace Gpib
{
    // ContecGPIB例外クラス
    public class ContecGpibException : Exception
    {
        public ContecGpibException(string message) : base(message) { }
    }

    /// <summary>
    /// Cgpibラッパークラス
    /// </summary>
    public class ContecGpib
    {
        /// <summary>
        /// クラスCgpibインスタンス作成
        /// </summary>
        Cgpib gpib = new Cgpib();

        private short devId; // デバイスID
        short[] eqpId = new short[32]; // イクイップメントID[物理アドレス]

        private static readonly short GpibDelim = 1;
        private static readonly short GpibEoi = 1;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="devName"></param>
        public ContecGpib(string devName)
        {
            int ret;
            int data;
            string strRet;

            //初期化関数実行
            ret = gpib.Init(devName, out devId);
            if (ret != 0)
            {
                //エラーの場合
                gpib.GetErrorString(ret, out strRet);
                throw new ContecGpibException(ret.ToString() + "\n" + strRet);
            }
            //正常にデバイスIDを取得できた場合
            //マスタに設定されているかどうかの確認
            ret = gpib.GetStatus(devId, 0x0A, out data);
            if ((ret == 0) && (data == 1))
            {
                throw new ContecGpibException(
                    "マスタに設定されていません。プロパティでマスタに設定してください。");
            }

            gpib.SendIFC(devId, 1);	//IFCを送出
            if (ret != 0)
            {
                //エラーの場合
                gpib.GetErrorString(ret, out strRet);
                throw new ContecGpibException(ret.ToString() + "\n" + strRet);
            }

            gpib.ChangeREN(devId, 1);
            if (ret != 0)
            {
                //エラーの場合
                gpib.GetErrorString(ret, out strRet);
                throw new ContecGpibException(ret.ToString() + "\n" + strRet);
            }
        }

        /// <summary>
        /// GPIB機器をオープン（イクイップメントID取得）
        /// </summary>
        /// <param name="addr">物理アドレス</param>
        public void Open(int addr)
        {
            funcSetParam((short)addr, ref eqpId[addr]);
        }

        /// <summary>
        /// 終了（デバイスID無効化）
        /// </summary>
        public void Close()
        {
            int ret;
            string strRet;

            ret = gpib.ChangeREN(devId, 0);
            if (ret != 0)
            {
                //エラーの場合
                gpib.GetErrorString(ret, out strRet);
                throw new ContecGpibException(ret.ToString() + "\n" + strRet);
            }

            ret = gpib.Exit(devId);
            if (ret != 0)
            {
                //エラーの場合
                gpib.GetErrorString(ret, out strRet);
                throw new ContecGpibException(ret.ToString() + "\n" + strRet);
            }
        }

        /// <summary>
        /// 相手機器検索
        /// </summary>
        /// <returns>ディクショナリ[物理アドレス, 機器名]</returns>
        public Dictionary<int, string> Search()
        {
            short[] addrArray = new short[32];
            short arraySize = (short)addrArray.Length;
            int ret;
            int sendLen;
            string recBuf;
            string strRet;
            string sendBuf;
            Dictionary<int, string> result = new Dictionary<int, string>(); // 検索結果

            ret = gpib.FindListener(devId, -1, 0, ref arraySize, addrArray);
            if (ret != 0)
            {
                //エラーの場合
                gpib.GetErrorString(ret, out strRet);
                throw new ContecGpibException(ret.ToString() + "\n" + strRet);
            }

            //検出された機器の機器情報を取得
            for (int i = 0; i < arraySize; i++)
            {
                //Equipent IDの取得
                ret = gpib.SetEquipment(devId, out eqpId[i], addrArray[i], 0, GpibDelim);
                if (ret != 0)
                {
                    //エラーの場合
                    gpib.GetErrorString(ret, out strRet);
                    continue;
                }

                //機器情報問い合わせ（送信）
                sendBuf = "*IDN?";	//送信データ
                sendLen = sendBuf.Length;	//送信データ数
                ret = gpib.SendData(eqpId[i], ref sendLen, sendBuf);
                if (ret != 0)
                {
                    //エラーの場合
                    gpib.GetErrorString(ret, out strRet);
                    continue;
                }

                //機器情報問い合わせ（受信）
                int recLen = 256;
                ret = gpib.RecData(eqpId[i], ref recLen, out recBuf);
                if (ret != 0)
                {
                    //エラーの場合
                    gpib.GetErrorString(ret, out strRet);
                    continue;
                }

                //受信データがIEEE488.2規格か確認
                if (recBuf.IndexOf(",") == -1)
                    recBuf = null; // 規格でなければアドレスのみ

                result.Add(addrArray[i], recBuf);
            }
            return result;
        }

        /// <summary>
        /// 送信
        /// </summary>
        /// <param name="addr">物理アドレス</param>
        /// <param name="command">送信文字列</param>
        public void Send(int addr, string command)
        {
            int sendLength = command.Length;
            string strRet;

            if (sendLength == 0)
            {
                strRet = "GpibSendData : 送信データがありません";
                throw new ContecGpibException(strRet);	//リストに表示
            }

            //送信実行
            int ret = gpib.SendData(eqpId[addr], ref sendLength, command);
            if (ret != 0)
            {
                //エラーの場合
                gpib.GetErrorString(ret, out strRet);
                throw new ContecGpibException(ret.ToString() + "\n" + strRet);
            }
        }

        /// <summary>
        /// 受信
        /// </summary>
        /// <param name="addr">物理アドレス</param>
        /// <returns>受信文字列</returns>
        public string Recieve(int addr)
        {
            int recLen = 1024;
            string strRet;
            string buf;

            //受信実行
            int ret = gpib.RecData(eqpId[addr], ref recLen, out buf);
            if (ret != 0)
            {
                //エラーの場合
                gpib.GetErrorString(ret, out strRet);
                throw new ContecGpibException(ret.ToString() + "\n" + strRet);
            }
            return buf;
        }

        /// <summary>
        /// ステータスバイト取得
        /// </summary>
        /// <param name="addr">物理アドレス</param>
        /// <param name="Stb">ステータスバイト</param>
        /// <param name="srqLine">SRQライン状態</param>
        public void GetStatusByte(int addr, out UInt16 Stb, out UInt16 srqLine)
        {
            string buf;
            short intStb;
            short intSrqLine;

            //シリアルポール函数の実行
            int ret = gpib.SPoll(eqpId[addr], out intStb, out intSrqLine);
            //戻り値処理
            if (ret != 0)
            {
                gpib.GetErrorString(ret, out buf);
                throw new ContecGpibException(ret.ToString() + "\n" + buf);
            }
            Stb = (UInt16)intStb;
            srqLine = (UInt16)intSrqLine;
        }

        /// <summary>
        /// EOI, デリミタ, タイムアウトの設定
        /// </summary>
        /// <param name="address"></param>
        /// <param name="eqpId"></param>
        private void funcSetParam(short address, ref short eqpId)
        {
            int ret;
            string strRet;

            ret = gpib.SetEquipment(devId, out eqpId, address, 0, GpibDelim);
            if (ret != 0)
            {
                //エラーの場合
                gpib.GetErrorString(ret, out strRet);
                throw new ContecGpibException(ret.ToString() + "\n" + strRet);
            }

            //デリミタコードとEOIラインの設定
            ret = gpib.SetDelim(eqpId, GpibDelim, GpibEoi, 0);
            if (ret != 0)
            {
                //エラーの場合
                gpib.GetErrorString(ret, out strRet);
                throw new ContecGpibException(ret.ToString() + "\n" + strRet);
            }

            ret = gpib.SetTimeOut(eqpId, 10000);
            if (ret != 0)
            {
                //エラーの場合
                gpib.GetErrorString(ret, out strRet);
                throw new ContecGpibException(ret.ToString() + "\n" + strRet);
            }
        }
    }
}
