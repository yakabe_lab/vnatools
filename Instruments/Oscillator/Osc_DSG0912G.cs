﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO.Ports;
using System.Diagnostics;
using System.Collections;

namespace Instruments.Oscillator
{
    /// <summary>
    /// 発振器：DST DSG-0912Gの制御
    /// http://www.dst.co.jp/product/products/dsg0912gj.html
    /// </summary>

    public class Osc_DSG0912G : IOscillator
    {
        /// <summary>
        /// SGの接続シリアルポート
        /// </summary>
        private SerialPort sgCom = new SerialPort();

        private bool ready = false;
        private string ComPort = null;

        /// <summary>
        /// 開いて接続チェック
        /// </summary>
        public Osc_DSG0912G(string portName)
        {
            if (sgCom.IsOpen) return;

            // COMxx形式じゃないと受け付けない
            if (string.Compare(ComPort, 0, "COM", 0, "COM".Length) != 0)
            {
                throw new ArgumentException(string.Format("接続ポート {0} は対応していません", ComPort));
            }

            this.ComPort = portName;
            sgCom = new SerialPort();
            sgCom.PortName = ComPort;
            sgCom.BaudRate = 9600;
            sgCom.NewLine = "\r\n";
            sgCom.DataBits = 8;
            sgCom.DiscardNull = false;
            sgCom.Encoding = Encoding.ASCII;
            sgCom.Handshake = Handshake.None;
            sgCom.Parity = Parity.None;
            sgCom.StopBits = StopBits.One;
            sgCom.ReadTimeout = 3000;
            sgCom.WriteTimeout = 3000;

            sgCom.Open();
            Debug.WriteLine("SGcom: open");
            sgCom.DiscardOutBuffer();
            sgCom.DiscardInBuffer();

            // 初期設定
            sendCommandAndErrorCheck("ECHO 0"); // Nonエコーバックモードに設定
            sendCommandAndErrorCheck("EOFF 0"); // 外部信号でRFをOFFにする機能をOFFにする(ソフトウェアでのON/OFFが有効になる)

            // 接続チェック
            if (!checkConnection())
            {
                Close();
                throw new InvalidOperationException(string.Format("SGが {0} に接続されていません", sgCom.PortName));
            }

            ready = true;
        }

        /// <summary>
        /// 閉じる
        /// </summary>
        public void Close()
        {
            ready = false;
            if (sgCom.IsOpen)
            {
                sgCom.Close();
                Debug.WriteLine("SGcom: close");
            }
        }

        public void Dispose()
        {
            Close();
        }

        /// <summary>
        /// SG出力のON, OFF
        /// </summary>
        /// <param name="on"></param>
        public void SetOutput(bool on)
        {
            if (!ready)
            {
                throw new InvalidOperationException("発振器がオープンされていません");
            }
            if (on)
            {
                sendCommandAndErrorCheck("ON");
            }
            else
            {
                sendCommandAndErrorCheck("OFF");
            }
            waitForOperationComplete();
        }

        /// <summary>
        /// SG出力電力の設定
        /// </summary>
        /// <param name="dBm"></param>
        public void SetPower(double dBm)
        {
            //throw new NotSupportedException("この発振器は電力を設定できません");
        }

        /// <summary>
        /// SG出力周波数設定
        /// </summary>
        /// <param name="freq"></param>
        public void SetFreq(long freq)
        {
            if (!ready)
            {
                throw new InvalidOperationException("発振器がオープンされていません");
            }

            if (freq < 9000000000L) freq = 9000000000L;
            if (freq > 12000000000L) freq = 12000000000L;

            long freqstep = freq / 250000L;

            sendCommandAndErrorCheck("F " + freqstep.ToString());
            waitForOperationComplete();
        }

        /// <summary>
        /// SGの接続チェック
        /// </summary>
        private bool checkConnection()
        {
            string res = "";
            try
            {
                res = sendCommandAndErrorCheck("STS");
            }
            catch (TimeoutException)
            {
                return false;
            }

            return res[0] == '*';
        }

        /// <summary>
        /// SGの操作(周波数変更や電力変更など)が終了したかどうかチェック
        /// (返事が返るまで待ってから帰る)
        /// </summary>
        /// <returns></returns>
        private bool waitForOperationComplete()
        {
            const int cntMax = 1000;
            int cnt;
            for (cnt = 0; cnt < cntMax; cnt++)
            {
                string res = sendCommandAndErrorCheck("STS");   // 1行目: *[CR][LF]
                string fstr = sgCom.ReadLine();                 // 2行目: xxxx[CR][LF]  周波数ステップ(36000～48000を16進で)
                string statflg = sgCom.ReadLine();              // 3行目: xx[CR][LF]    ステータスフラグ(16進)

                byte stat = Convert.ToByte(statflg, 16);
                BitArray bits = new BitArray(new byte[] { stat });
                if (bits[7] == false) break;                    // PLL状態がLockならbits[7]がfalse
            }
            if (cnt >= cntMax)
            {
                throw new Exception("DST発振器のPLLが時間内にLockしませんでした");
            }

            return true;
        }

        /// <summary>
        /// コマンド送信とエラーチェック, 受信
        /// </summary>
        /// <param name="cmd"></param>
        private string sendCommandAndErrorCheck(string cmd)
        {
            sgCom.DiscardInBuffer();
            sgCom.WriteLine(cmd);
            string res = sgCom.ReadLine();
            if (res[0] == '?')
            {
                // ?02 ←?の後に2文字の16進数値
                byte errcode = Convert.ToByte(res.Substring(1), 16);
                BitArray bits = new BitArray(new byte[] { errcode });
                string err = "";

                if (bits[0]) { err += "Command, "; }
                if (bits[1]) { err += "Parameter, "; }
                if (bits[2]) { err += "Setting, "; }
                if (bits[3]) { err += "Setting save, "; }
                if (bits[4]) { err += "Serial receive buffer overflow, "; }
                if (bits[5]) { err += "error 5, "; }
                if (bits[6]) { err += "error 6, "; }
                if (bits[7]) { err += "error 7, "; }

                Close();
                throw new Exception("SG Error: " + err);
            }
            return res;
        }

    }
}
