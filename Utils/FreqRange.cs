﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utils
{
    /// <summary>
    /// 周波数の範囲を格納しておく
    /// </summary>
    public class FreqRange : IEquatable<FreqRange>
    {
        public long Max { get; private set; }
        public long Min { get; private set; }
        public int Count { get; private set; }
        public long Step { get; private set; }
        public List<long> FreqList { get; private set; }

        public FreqRange(long min, long max, int count)
        {
            this.Min = min;
            this.Max = max;
            this.Count = count;
            this.createFreqList();
            this.Step = (Count <= 1) ? 0 : (Max - Min) / (Count - 1);
        }

        public long this[int i]
        {
            get { return FreqList[i]; }
        }

        public IEnumerator<long> GetEnumerator()
        {
            foreach (var i in FreqList)
                yield return i;
        }

        /// <summary>
        /// 周波数配列を生成
        /// </summary>
        private void createFreqList()
        {
            FreqList = new List<long>(Count);

            if (Count == 1)
            {
                FreqList.Add(Min);
                return;
            }
            for (int i = 0; i < Count; i++)
            {
                FreqList.Add(Min + (Max - Min) / (Count - 1) * i);
            }
        }

        public static bool operator ==(FreqRange a, FreqRange b)
        {
            return a.Equals(b);
        }

        public static bool operator !=(FreqRange a, FreqRange b)
        {
            return !(a == b);
        }

        public override bool Equals(object right)
        {
            if (object.ReferenceEquals(right, null))
                return false;
            if (object.ReferenceEquals(this, right))
                return true;
            if (GetType() != right.GetType())
                return false;

            return this.Equals(right as FreqRange);
        }

        public bool Equals(FreqRange right)
        {
            return (this.Max == right.Max && this.Min == right.Min && this.Step == right.Step);
        }

        public override int GetHashCode()
        {
            return (Max ^ Min ^ Step).GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("({0}, {1}) {2}", Min, Max, Count);
        }
    }
}
