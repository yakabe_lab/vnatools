﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;

namespace Instruments.TestSet
{
    public class Ts_Dummy : ITestSet
    {
        public Ts_Dummy() { }

        public void Dispose() { }

        /// <summary>
        /// テストセット切り替え
        /// </summary>
        /// <param name="set"></param>
        public void SetState(SwitchState s, int output = 0, int input = 0) { }
    }
}
