﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Management;
using System.Numerics;

namespace Utils
{
    /// <summary>
    /// 雑多なstaticメソッドを集めたクラス
    /// </summary>
    public class Misc
    {
        /// <summary>
        /// コメント行を飛ばした位置のストリームを返す
        /// </summary>
        /// <param name="fs">ファイルストリーム</param>
        /// <param name="commentChar">コメントの開始文字</param>
        /// <returns>シーク後のストリーム</returns>
        public static FileStream SkipCommentRow(FileStream fs, string commentChar)
        {
            using (var sr = new StreamReader(fs))
            {
                string line = null;
                do
                {
                    long pos = fs.Position;
                    line = sr.ReadLine();
                    if (commentChar.Any(c => line[0] != c))
                    {
                        fs.Seek(pos, SeekOrigin.Begin);
                        break;
                    }
                } while (line == null);
            }
            return fs;
        }

        // ひろいもの
        public static string[] GetDeviceNames()
        {
            var deviceNameList = new System.Collections.ArrayList();
            var check = new System.Text.RegularExpressions.Regex("(COM[1-9][0-9]?[0-9]?)");

            ManagementClass mcPnPEntity = new ManagementClass("Win32_PnPEntity");
            ManagementObjectCollection manageObjCol = mcPnPEntity.GetInstances();

            //全てのPnPデバイスを探索しシリアル通信が行われるデバイスを随時追加する
            foreach (ManagementObject manageObj in manageObjCol)
            {
                //Nameプロパティを取得
                var namePropertyValue = manageObj.GetPropertyValue("Name");
                if (namePropertyValue == null)
                {
                    continue;
                }

                //Nameプロパティ文字列の一部が"(COM1)～(COM999)"と一致するときリストに追加"
                string name = namePropertyValue.ToString();
                if (check.IsMatch(name))
                {
                    deviceNameList.Add(name);
                }
            }

            //戻り値作成
            if (deviceNameList.Count > 0)
            {
                string[] deviceNames = new string[deviceNameList.Count];
                int index = 0;
                foreach (var name in deviceNameList)
                {
                    deviceNames[index++] = name.ToString();
                }
                return deviceNames;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// COMポート番号の抽出
        /// </summary>
        /// <param name="name"></param>
        public static string ExtractCom(string name)
        {
            int start = name.IndexOf('(') + 1;
            int last = name.IndexOf(')') + 1;
            return name.Substring(start, last - start - 1);
        }

        public static int ExtractComNum(string name)
        {
            string buf = ExtractCom(name);
            return int.Parse(buf.Substring(3));
        }

        /// <summary>
        /// 2のn乗か調べる
        /// http://stackoverflow.com/questions/600293/how-to-check-if-a-number-is-a-power-of-2
        /// </summary>
        /// <param name="x">調べる数</param>
        /// <returns>2のn乗か</returns>
        public static bool IsPowerOfTwo(int x)
        {
            return (x != 0) && ((x & (x - 1)) == 0);
        }

        /// <summary>
        /// 立っているビットを数える
        /// </summary>
        /// <param name="n">調べる数</param>
        /// <returns>立っているビット数</returns>
        public static int CountSetBits(int n)
        {
             int count = 0;
             while (n == 0)
             {
                 count += n & 1;
                 n >>= 1;
             }
             return count;
        }

        /// <summary>
        /// 最下位ビットから0の連続を数える
        /// Number of Training Zeros
        /// </summary>
        public static int NTZ(int n)
        {
            return CountSetBits((n & (-n)) - 1);
        }

        /// <summary>
        /// dBmをmWに変換
        /// </summary>
        /// <param name="dbm"></param>
        /// <returns></returns>
        public static double dBm2mW(double dbm)
        {
            return Math.Pow(10, dbm / 10.0);
        }

        public static Complex MagDegToComplex(double dB, double deg)
        {
            double abs = Math.Pow(10.0, dB / 20.0);
            double rad = deg / 180.0 * Math.PI;
            return Complex.FromPolarCoordinates(abs, rad);
        }

        public static double ComplexToLogMag(Complex c)
        {
            return 20.0 * Math.Log10(c.Magnitude);
        }

        public static double ComplexToDegree(Complex c)
        {
            return c.Phase * 180.0 / Math.PI;
        }

        public static double DelayToDegree(double sec, double Hz)
        {
            return -2.0 * Math.PI * sec * Hz;
        }

        public static bool IsEmptyDirectory(string dir)
        {
            if (!Directory.Exists(dir))
            {
                // ディレクトリが存在しなければ空でないとする
                return false;
            }
            try
            {
                string[] entries = Directory.GetFileSystemEntries(dir);
                return entries.Length == 0;
            }
            catch
            {
                // アクセス権がないなどの場合は空でないとする
                return false;
            }
        }
    }
}
