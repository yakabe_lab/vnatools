﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Gpib;
using Instruments.Oscillator;
using Instruments.PowerMeter;
using Instruments.TestSet;
using Instruments.PhaseShifter;
using Instruments.Sweeper;

namespace Instruments
{
    public enum SwitchState { DUT, CAL, TBL, Ref, CalRef };
    public enum InstType { Osc, Pm, Ts, Ps };

    public static class InstDefs
    {
        public static readonly Dictionary<string, Func<ContecGpib, int, IInstrument>> Osc
            = new Dictionary<string, Func<ContecGpib, int, IInstrument>>()
            {
                {"E8257D", (gpib, port) => new Osc_E8257D(gpib, port)},
                {"DSG0912G", (gpib, port) =>  new Osc_DSG0912G("COM" + port.ToString())},
                {"Dummy", (gpib, port) =>  new Osc_Dummy()},
            };
        public static readonly Dictionary<string, Func<ContecGpib, int, IInstrument>> Pm
            = new Dictionary<string, Func<ContecGpib, int, IInstrument>>()
            {
                {"NRP2", (gpib, port) => new Pm_NRP2(gpib, port)},
                {"安井ADC", (gpib, port) =>  new Pm_ADC_Yasui("COM" + port.ToString(), "Table")},
                {"電力ファイルから読み込み", (gpib, port) =>  new Swp_FromFile()},
                {"Dummy", (gpib, port) =>  new Pm_Dummy()},
            };
        public static readonly Dictionary<string, Func<ContecGpib, int, IInstrument>> Ts
            = new Dictionary<string, Func<ContecGpib, int, IInstrument>>()
            {
                {"廣瀬松浦テストセット", (gpib, port) => new Ts_HMTestSet(port)},
                {"手動変更", (gpib, port) => new Ts_Manual()},
                {"Dummy", (gpib, port) =>  new Ts_Dummy()},
            };

        public static readonly Dictionary<string, Func<ContecGpib, int, IInstrument>> Ps
            = new Dictionary<string, Func<ContecGpib, int, IInstrument>>()
            {
                {"Wakaフェーズシフタ", (gpib, port) => new Ps_Waka(port, true)},
                {"Dummy", (gpib, port) =>  new Ps_Dummy()},
            };

        public static readonly Dictionary<InstType, Dictionary<string, Func<ContecGpib, int, IInstrument>>> InstDicts
            = new Dictionary<InstType, Dictionary<string, Func<ContecGpib, int, IInstrument>>>()
            {
                {InstType.Osc, InstDefs.Osc}, {InstType.Pm, InstDefs.Pm},
                {InstType.Ts, InstDefs.Ts}, {InstType.Ps, InstDefs.Ps},
            };
    }
}
