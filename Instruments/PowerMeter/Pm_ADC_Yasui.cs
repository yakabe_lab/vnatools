﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO.Ports;
using Instruments.TestSet;
using Utils;

namespace Instruments.PowerMeter
{
    public class Pm_ADC_Yasui : IPowerMeter, ITestSet
    {
        // 接続状態を返す
        public bool Connection { get; protected set; }

        public int PortCount { get { return portCount; } }

        private static readonly int portCount = 4;

        private CSVTable PTable;
        private SerialPort com;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public Pm_ADC_Yasui(String PortName, string path)
        {
            if (!System.IO.File.Exists(path))
            {
                //MessageBox.Show(path + "\r\nファイルが存在しません。");
                throw new Exception(path + "\r\nファイルが存在しません。");
                //return;
            }
            PTable = new CSVTable(path);

            try
            {
                com = new SerialPort(Misc.ExtractCom(PortName), 115200);
                com.ReadTimeout = 3000;

                // 接続
                com.Open();

                // 正常な機器か確認
                com.WriteLine("*");
                string tmp = com.ReadLine();
                if (tmp.IndexOf("24bit_ADC_controller_V2") < 0)
                    throw new Exception("不正な機器");

                // 接続状態の確立
                Connection = true;
                // Config.ADCPortName = PortName;
                refVoltage = 1.2412;  // 適切な比較電圧を設定する

                setSpeed(3);
            }
            catch
            {
                Connection = false;
                //MessageBox.Show("機器への接続に失敗しました");
                if (com.IsOpen)
                    this.Close();
                throw new Exception("機器への接続に失敗しました");
            }
        }

        /// <summary>
        /// 機器の切断
        /// </summary>
        public void Close()
        {
            com.Close();
            Connection = false;
        }

        public void Dispose()
        {
            Close();
        }

        public double[] GetPower(long freq, string msg = "")
        {
            double[] voltages = getADC_Voltage();
            return ConvertPower(freq, voltages);
        }

        // -------------------- 以下機器別処理 -----------------------------

        /// <summary>
        /// 比較電圧値
        /// </summary>
        public double refVoltage { get; set; }  // 比較電圧

        /// <summary>
        /// ADC電圧値と電力変換テーブルの値を使って電力値を計算して返す
        /// </summary>
        /// <param name="freq">周波数値</param>
        /// <param name="ADCVol">元になる電圧値</param>
        /// <returns></returns>
        private double[] ConvertPower(long freq, double[] ADCVol)
        {
            double[] res = new double[4];


            for (int i = 0; i < 4; i++)
            {
                double vol = ADCVol[4] - ADCVol[i]; // 検索対象の電圧
                int idx1 = 0, idx2 = PTable.RowCount() - 1;
                double min = PTable[idx1][5] - PTable[idx1][i + 1]; // 基準差分
                double max = PTable[idx2][5] - PTable[idx2][i + 1]; // 基準差分

                // 範囲確認
                if ((vol < min) || (vol > max))
                {
                    //MessageBox.Show(String.Format("範囲外エラー：Port{0}", i + 3));
                    res[i] = -1;
                }
                else
                {
                    // idx1 と idx2が隣り合うまで検索を続ける
                    while (idx1 != (idx2 - 1))
                    {
                        int Meanidx = (idx1 + idx2) / 2;
                        double mean = PTable[Meanidx][5] - PTable[Meanidx][i + 1];

                        if (vol > mean)
                            idx1 = Meanidx;
                        else
                            idx2 = Meanidx;
                    }

                    // 1次線形保管
                    double Pow1 = PTable[idx1][0], Pow2 = PTable[idx2][0];
                    min = PTable[idx1][5] - PTable[idx1][i + 1]; // 基準差分
                    max = PTable[idx2][5] - PTable[idx2][i + 1]; // 基準差分
                    res[i] = (vol - min) * (Pow2 - Pow1) / (max - min) + Pow1;
                }
            }
            return res;
        }

        /// <summary>
        /// 電圧値の取得
        /// </summary>
        /// <returns></returns>
        public double[] getADC_Voltage()
        {
            com.WriteLine("G");
            string tmp = com.ReadLine();

            // 取得文字列を , 区切りの文字列にし、電圧値に変換して返す
            return Array.ConvertAll<string, double>(tmp.Split(','),
                s => ((double)Convert.ToInt32(s, 16)) * refVoltage / (double)(Convert.ToInt32("0x00FFFFFF", 16)));
        }

        public String getADC_Voltage_Str()
        {
            return String.Join(",",
                Array.ConvertAll<double, string>(getADC_Voltage(), x => x.ToString()));
        }

        /// <summary>
        /// 変換スピードの設定
        /// </summary>
        /// <param name="s"></param>
        public void setSpeed(int s)
        {
            com.WriteLine("s" + s);
        }

        /// <summary>
        /// 拡張ポートの設定
        /// </summary>
        /// <param name="ex"></param>
        public void setExPort(int ex)
        {
            com.WriteLine("!" + ex.ToString("X2"));
        }

        public void SetState(SwitchState s, int output = 0, int input = 0)
        {
            // 出力側SW
            // 2 1  ->  01 00  ->  P1 TS短絡
            // 3 4  ->  10 11  ->  P2 Match
            // 入力側SW
            // 1 2  ->  00 01  ->  P1 TS短絡
            // 4 3  ->  11 10  ->  P2 Match
            // 00[出力側SW]00[入力側SW]
            // DUT:
            //  S11 -> 0100 : 4
            //  S21 -> 1000 : 8
            //  S12 -> 0111 : 7
            //  S22 -> 1011 : 11
            // CalRef:
            // Ref:
            //  Match接続 -> 1101 : 13
            // CAL:
            //  テストセット短絡経路 -> 0001 : 1
            int[,] swMatrix = new int[,] { { 4, 7 }, { 8, 11 } };
            int ex = 0;
            if (input > 2 || output > 2)
                throw new Exception("テストスイッチのポート設定範囲外です");

            switch (s)
            {
                case SwitchState.CAL:
                    ex = 1;
                    break;
                case SwitchState.CalRef:
                case SwitchState.Ref:
                    ex = 13;
                    break;
                case SwitchState.DUT:
                    ex = swMatrix[output, input];
                    break;
                default:
                    throw new Exception("定義されていないテストスイッチ状態");
            }
            setExPort(ex);
        }
    }
}