﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;

namespace Utils
{
    public class PortPowerFile
    {
        public FreqRange FreqRange { private set; get; }
        public int PortCount { private set; get; }

        // 電力データ[周波数][ポート]
        private double[][] data;

        public double[] this[int i]
        {
            get { return data[i]; }
        }

        public PortPowerFile(string fileName)
        {
            var freqList = new List<long>();
            var tempData = new List<double[]>();
            var powerList = new List<double>();
            string errMsg = fileName + Environment.NewLine
                + "読み込みエラー{0}行目" + Environment.NewLine + "{1}";

            var lines = File.ReadAllLines(fileName);
            for (int l = 0; l < lines.Length; l++)
            {
                if (lines[l].StartsWith("#")) // コメント行
                    continue;
                
                string[] values = lines[l].Split(',');
                if (values.Length < 5) // ポート数が4以下
                    throw new Exception(string.Format(errMsg, l, lines[l]));
                long freq;
                if (!long.TryParse(values[0], out freq))
                    throw new Exception(string.Format(errMsg, l, lines[l]));
                freqList.Add(freq);

                powerList.Clear();
                foreach (var val in values.Skip(1)) // 周波数は飛ばす
                {
                    double pow;
                    if (!double.TryParse(val, out pow))
                        throw new Exception(string.Format(errMsg, l, lines[l]));
                    powerList.Add(pow);
                }
                tempData.Add(powerList.ToArray());
            }
            PortCount = tempData[0].Length;
            if (!tempData.All(pows => pows.Length == PortCount))
                throw new Exception("ポート数が揃っていません");
            FreqRange = new FreqRange(freqList.Min(), freqList.Max(), freqList.Count);
            data = tempData.ToArray();
        }

        public PortPowerFile(double[][] powerData, FreqRange fr, int portCount = 4)
        {
            data = powerData;
            FreqRange = fr;
            PortCount = portCount;
        }

        public void Save(string fileName)
        {
            string typeStr = "# F" + new string('P', PortCount);
            string header = "# Freq[Hz]"
                + string.Join("", Enumerable.Repeat(3, PortCount).Select(i => ", P" + i + "[mW]"));
            string comment =
                typeStr + Environment.NewLine +
                "# (F=Freq, P=Power, V=RawData)" + Environment.NewLine +
                "# " + DateTime.Now.ToString() + Environment.NewLine +
                header + Environment.NewLine;

            using (var sw = new StreamWriter(fileName))
            {
                sw.Write(comment);
                for (int f = 0; f < FreqRange.Count; f++)
                {
                    sw.WriteLine(FreqRange[f] + "," + string.Join(",", data[f]));
                }
            }
        }
    }
}
