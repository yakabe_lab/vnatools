﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Instruments.TestSet
{
    public interface ITestSet : IDisposable, IInstrument
    {
        void SetState(SwitchState state, int input = 0, int output = 0);
    }
}
