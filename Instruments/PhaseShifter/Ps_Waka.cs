﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO.Ports;
using System.Threading;
using System.Diagnostics;

namespace Instruments.PhaseShifter
{
    public enum Direction { Up, Down };

    public class Ps_Waka : IPhaseShifter
    {
        public bool isHarfStep;
        public bool cdChanged; // CDが変化（モータが限界位置）
        public int StepMX { private set; get; } // 校正ステップ単位
        public int UnitStep { private set; get; } // 単位ステップ ハーフ:1, フル:2
        public double Shift { private set; get; } // 相対位置
        public double Delay { private set; get; } // 相対遅延
        public double AbsoluteDelay { private set; get; } // 絶対遅延（あんまり信用ならない？）
        public int StopSetMax { private set; get; } //左方向の位置
        public int StartSetMin { private set; get; } // 右方向の位置
        public int Step { private set; get; } // 積算ステップ

        private SerialPort com;

        static readonly int motorStep = 200;
        static readonly int maxCalStep = 4167;
        static readonly double delayOffset = 689;
        // 変位[mm] / 遅延[ps]
        // 2(往復) * [ps/s] / (光速c[m/s] * [mm/m])
        static readonly double shiftPerDelay = 2 * 1e12 / (2.99792458e8 * 1e3);

        public bool IsHarfStep
        {
            set
            {
                if (value == isHarfStep) return;
                isHarfStep = value;
                UnitStep = (value) ? 2 : 1;
                if (value)
                {
                    Step *= 2;
                    StopSetMax *= 2;
                }
                else
                {
                    Step /= 2;
                    StopSetMax /= 2;
                }
                com.DiscardInBuffer();
                Thread.Sleep(200);
                string buf = "0M";
                buf += (value) ? "2\r" : "0\r";
                com.Write(buf);
            }
            get { return isHarfStep; }
        }

        private void wakaDriver_PinChanged(object sender, SerialPinChangedEventArgs e)
        {
            if (e.EventType == SerialPinChange.CDChanged)
                this.cdChanged = true;
        }

        public Ps_Waka(int port, bool isHarfStep = true)
        {
            com = new SerialPort("COM" + port, 9600, Parity.None, 8, StopBits.One);
            string buf;
            this.StartSetMin = 0;
            this.cdChanged = false;
            com.PinChanged += wakaDriver_PinChanged;

            Stopwatch sw = new Stopwatch();
            com.Open();
            com.RtsEnable = true;
            com.Write("\r");
            sw.Start();
            do
            {
                if (sw.ElapsedMilliseconds > 1000)
                {
                    throw new Exception("移相器接続エラー");
                }
                buf = com.ReadExisting();
            } while (buf.IndexOf("WAKA0") == -1);

            this.IsHarfStep = isHarfStep;
            this.StepMX = 400 * UnitStep;
            buf = string.Format("0P{0}\r", 11950 / 250 / UnitStep);
            com.Write(buf);
            this.setOrigin(30);
        }

        public void Dispose()
        {
            if (com.IsOpen)
            {
                com.Close();
            }
        }

        private void calcPosition()
        {
            Shift = (double)Step / motorStep / UnitStep;
            Delay = Shift * shiftPerDelay;
            AbsoluteDelay = Delay + delayOffset;
        }

        public double SetDelayDifference(double delay)
        {
            return SetDelay(delay - this.Delay);
        }

        public double SetDelay(double delay)
        {
            SetAbsoluteStep(Convert.ToInt32(delay * motorStep * UnitStep / shiftPerDelay));
            return this.Delay;
        }

        public void SetAbsoluteStep(int step)
        {
            SetRelativeStep(step - Step);
        }

        public void SetRelativeStep(int step)
        {
            if (Step + step < StartSetMin || Step + step > StopSetMax || step == Step)
                return;
            else if (step > 0)
                Rotate(step, Direction.Up);
            else
                Rotate(-step, Direction.Down);
        }

        public void Rotate(int step, Direction d)
        {
            com.DtrEnable = true; // DTR=H モータ電源ON
            string str = "0" + ((d == Direction.Up) ? "L" : "R") + step.ToString() + "\r";
            string buf = "";
            com.Write(str);

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            do // 原点付近のエラー 原点でモータが止まらない
            {
                if (cdChanged)
                {
                    com.DtrEnable = false;
                    Stop();
                    Thread.Sleep(500);
                    com.DtrEnable = true;
                    str = "0" + ((d == Direction.Up) ? "R" : "L") + "200\r";
                    com.Write(str);
                    Thread.Sleep(500);
                    com.DtrEnable = false;
                    cdChanged = false;
                    throw new Exception("移相器設定範囲外");
                }
                if (stopWatch.ElapsedMilliseconds / 20 > StepMX + 500)
                {
                    throw new Exception("移相器接続エラー");
                }
                buf += com.ReadExisting();
            } while (buf.IndexOf("000A00000\r") == -1);

            Step += (d == Direction.Up) ? step : -step;
            com.DiscardInBuffer();

            if (Step > StopSetMax)
                StopSetMax = Step;
            if (Step < StartSetMin)
                StartSetMin = Step;
            calcPosition();
            com.DtrEnable = false;
        }

        private void setOrigin(int originOfset)
        {
            com.DtrEnable = true; // DTR=H モータ電源ON
            com.DiscardInBuffer();
            Thread.Sleep(500);
            int stepX1 = 1; // 原点SWエラー検出のセット

            // モータをR原点1に進める 最小遅延時間
            for (; ; )
            {
                if (cdChanged)
                {
                    cdChanged = false;
                    break;
                }

                rotateForCal(StepMX, Direction.Down);
                stepX1++; // 原点SWのエラー検出

                if (stepX1 > maxCalStep * UnitStep / StepMX + 2)
                {
                    rotateForCal(StepMX * UnitStep, Direction.Up);
                    com.DtrEnable = false; // DTR=L モータ電源OFF
                    throw new Exception("移相器原点検出エラー");
                }
            }
            stepX1 = 1;
            Stop();
            com.DtrEnable = false; // DTR=L モータ電源OFF
            Thread.Sleep(500);
            com.DiscardInBuffer();
            Thread.Sleep(500);
            com.DtrEnable = true; // DTR=H モータ電源ON
            Thread.Sleep(500);

            // R原点1のオフセット分の逃げ
            rotateForCal(originOfset * UnitStep * 4, Direction.Up);
            cdChanged = false;
            Step = originOfset * UnitStep * 4;

            com.DiscardInBuffer();
            Thread.Sleep(500);

            // モータの1ステップ送り 原点1の検出開始
            stepX1 = 1; // 原点SWエラー検出のセット
            for (; ; )
            {
                // スイッチ読み取り L>H で検出
                if (cdChanged)
                {
                    cdChanged = false;
                    break;
                }
                rotateForCal(1, Direction.Down);
                Thread.Sleep(10);
                stepX1++; // 原点SWのエラー検出
                if (stepX1 > originOfset * UnitStep * 4)
                {
                    com.DtrEnable = false; // DTR=L モータ電源OFF
                    throw new Exception("移相器原点検出エラー");
                }
                calcPosition();
                Step--;
            }
            stepX1 = 1; // 原点SWエラー検出のセット
            Thread.Sleep(500);
            com.DiscardInBuffer();
            Thread.Sleep(500);
            // R原点1スイッチMINの逃げ
            rotateForCal(originOfset * UnitStep, Direction.Up);
            Stop(); // モータ停止
            com.DtrEnable = false; // DTR=L モータ電源OFF
            cdChanged = false;

            // 表示をセット
            Step = 0;
            StartSetMin = 0;
            StopSetMax = 0;

            // モータをL原点に進める 2
            int sleepTime = 1000;
            int lmStep = maxCalStep * UnitStep;
            com.DiscardInBuffer();
            Thread.Sleep(500);
            com.DtrEnable = true; // DTR=H モータ電源ON
            for (; ; )
            {
                // スイッチ読み取り L>H で検出
                if (cdChanged)
                {
                    cdChanged = false;
                    break;
                }
                rotateForCal(lmStep, Direction.Up, sleepTime);

                calcPosition();

                Step += lmStep;
                lmStep = 1;
                sleepTime = 20;
            }
            Stop();
            com.DtrEnable = false; // DTR=L モータ電源OFF

            // L原点スイッチの逃げ調整
            Thread.Sleep(500);
            com.DtrEnable = true; // DTR=H モータ電源ON
            Thread.Sleep(500);
            rotateForCal(originOfset * UnitStep, Direction.Down, 500);
            cdChanged = false;
            Stop();
            com.DtrEnable = false; // DTR=L モータ電源OFF
            Thread.Sleep(500);
            com.DiscardInBuffer();
            int sumStepX1 = Step - originOfset * 2 * UnitStep;
            Step = sumStepX1 / 2 * 2;

            calcPosition();
            StopSetMax = Step;
            cdChanged = false;
        }

        private void rotateForCal(int step, Direction d, int sleepTime = 0)
        {
            string buf = "";
            string sendStr = (d == Direction.Up) ?
                string.Format("0L{0}\r", step) : string.Format("0R{0}\r", step);
            Stopwatch stopWatch = new Stopwatch();
            com.Write(sendStr); // モータをxxステップ回転
            Thread.Sleep(sleepTime);
            stopWatch.Start();
            do
            {
                if (stopWatch.ElapsedMilliseconds / 20 > StepMX + 500)
                {
                    com.DtrEnable = false;
                    throw new Exception("移相器接続エラー");
                }
                buf += com.ReadExisting();
            } while (buf.IndexOf("000A00000\r") == -1);
        }

        public void Stop()
        {
            com.Write("0S\r");
        }
    }
}
