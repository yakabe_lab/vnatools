﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Gpib;
using Utils;

namespace Instruments.PowerMeter
{
    public class Pm_NRP2 : IPowerMeter
    {
        private ContecGpib gpib; // GPIBラッパクラス
        private int addr; // 物理アドレス

        private static readonly int portCount = 4;

        public int PortCount { get { return portCount; } }

        // コンストラクタ
        public Pm_NRP2(ContecGpib gpib, int addr)
        {
            this.gpib = gpib;
            this.addr = addr;
            gpib.Open(addr);

            // 初期化
            gpib.Send(addr, "*RST");
            gpib.Send(addr, "*CLS");
            for (int i = 0; i < portCount; i++)
            {
                gpib.Send(addr,
                    string.Format("CONF{0} DEF,0.01,(@{1})", i + 1, i + 1));
            }
        }

        public void Dispose() { }

        public double[] GetPower(long freq, string msg = "")
        {
            double[] rawData = GetRawPower(freq);
            return rawData.Select(p => Misc.dBm2mW(p)).ToArray(); // mWに変換
        }

        public double[] GetRawPower(long freq)
        {
            char[] splitChar = { ',', '\r', '\n' };
            double[] data = new double[portCount];

            for (int i = 0; i < portCount; i++)
            {
                gpib.Send(addr, string.Format("SENS{0}:FREQ {1}", i + 1, (double)freq));
            }

            gpib.Send(addr, "INIT:ALL"); // 測定
            // 読み取り
            for (int i = 0; i < portCount; i++)
            {
                gpib.Send(addr, "FETC" + (i + 1) + "?");
                string value = gpib.Recieve(addr).Split(splitChar)[0];
                data[i] = double.Parse(value);
            }
            return data;
        }
    }
}
