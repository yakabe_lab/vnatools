﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Gpib;
using Utils;
using Instruments.Oscillator;
using Instruments.PowerMeter;
using Instruments.PhaseShifter;
using Instruments.TestSet;
using Instruments.Sweeper;

namespace Instruments
{
    public class DeviceManager : IDisposable
    {
        public IOscillator Osc { get; private set; }
        public IPowerMeter Pm { get; private set; }
        public ITestSet Ts { get; private set; }
        public IPhaseShifter Ps { get; private set; }
        private ContecGpib gpib = null;

        public DeviceManager(Dictionary<InstType, DeviceSetting> ds, FreqRange fr)
        {
            try
            {
                gpib = new ContecGpib("GPIB000");
            }
            catch (ContecGpibException)
            {
                gpib = null;
            }

            Osc = InstDefs.Osc[ds[InstType.Osc].Name](gpib, ds[InstType.Osc].PortNum) as IOscillator;
            Pm = InstDefs.Pm[ds[InstType.Pm].Name](gpib, ds[InstType.Pm].PortNum) as IPowerMeter;
            Ts = InstDefs.Ts[ds[InstType.Ts].Name](gpib, ds[InstType.Pm].PortNum) as ITestSet;
            Ps = InstDefs.Ps[ds[InstType.Ps].Name](gpib, ds[InstType.Pm].PortNum) as IPhaseShifter;

            if (Pm is ISweeper)
            {
                ISweeper sweeper = Pm as ISweeper;
                Osc = sweeper;
                sweeper.SetFrequencyRange(fr);
            }
        }

        public void Dispose()
        {
            Osc.Dispose();
            Pm.Dispose();
            Ts.Dispose();
            Ps.Dispose();
            if (gpib != null)
                gpib.Close();
        }
    }
}
