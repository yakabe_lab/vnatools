﻿//*************************************************************************************************
//	API-GPIB(WDM)																			Ver1.01
//	The header file for GPIB
//	CgpibCs.cs																		CONTEC.CO.,LTD.
//*************************************************************************************************


using System;
using System.Text;
using System.Runtime.InteropServices;

namespace CgpibCs
{
    /// <summary>
    /// Cgpib の概要の説明です。
    /// </summary>
    public class Cgpib
    {
        /// <summary>
        /// アンマネージDLL(CGPIB.DLL)のインポート
        /// </summary>

        [DllImport("CGPIB.DLL")]
        static extern int GpibInit(string pchDeviceName, ref short pshDevId);
        [DllImport("CGPIB.DLL")]
        static extern int GpibExit(short shDevId);
        [DllImport("CGPIB.DLL")]
        static extern int GpibGetErrorString(int lnErrorCode, System.Text.StringBuilder pchErrorString);

        //Initialization
        [DllImport("CGPIB.DLL")]
        static extern int GpibSetEquipment(short shDevId, ref short pshEqpId, short shPrmAddr, short shScdAddr, short shDelim);
        [DllImport("CGPIB.DLL")]
        static extern int GpibGetEquipment(short shEqpId, ref short pshPrmAddr, ref short pshScdAddr, ref short pshDelim);
        [DllImport("CGPIB.DLL")]
        static extern int GpibSendIFC(short shDevId, short shIfcTime);
        [DllImport("CGPIB.DLL")]
        static extern int GpibChangeREN(short shDevId, short shEnable);
        [DllImport("CGPIB.DLL")]
        static extern int GpibEnableRemote(short shId);

        //Send and Reception
        [DllImport("CGPIB.DLL")]
        static extern int GpibSendData(short shId, ref int plnSendLen, string pchSendBuf);
        [DllImport("CGPIB.DLL")]
        static extern int GpibSendData(short shId, ref int plnSendLen, [MarshalAs(UnmanagedType.LPArray)] byte[] pbSendBuf);
        [DllImport("CGPIB.DLL")]
        static extern int GpibRecData(short shId, ref int plnRecLen, System.Text.StringBuilder pchRecBuf);
        [DllImport("CGPIB.DLL")]
        static extern int GpibRecData(short shId, ref int plnRecLen, [MarshalAs(UnmanagedType.LPArray)] byte[] pbRecBuf);
        [DllImport("CGPIB.DLL")]
        static extern int GpibSetDelim(short shId, short shDelim, short shEoi, short shEos);
        [DllImport("CGPIB.DLL")]
        static extern int GpibGetDelim(short shId, ref short pshDelim, ref short pshEoi, ref short pshEos);
        [DllImport("CGPIB.DLL")]
        static extern int GpibSetTimeOut(short shDevId, int lnTimeOut);
        [DllImport("CGPIB.DLL")]
        static extern int GpibGetTimeOut(short shDevId, ref int plnTimeOut);
        [DllImport("CGPIB.DLL")]
        static extern int GpibSetEscape(short shDevId, short shEnable, short shKeyType, int lnKeyCode);
        [DllImport("CGPIB.DLL")]
        static extern int GpibGetEscape(short shDevId, ref short pshEnable, short shKeyType, ref int plnKeyCode);
        [DllImport("CGPIB.DLL")]
        static extern int GpibSetSlowMode(short shDevId, short shSlowTime);
        [DllImport("CGPIB.DLL")]
        static extern int GpibGetSlowMode(short shDevId, ref short pshSlowTime);
        [DllImport("CGPIB.DLL")]
        static extern int GpibSetSmoothMode(short shDevId, short shMode);
        [DllImport("CGPIB.DLL")]
        static extern int GpibGetSmoothMode(short shDevId, ref short pshMode);
        [DllImport("CGPIB.DLL")]
        static extern int GpibSetAddrInfo(short shDevId, short shTalker, [MarshalAs(UnmanagedType.LPArray)] short[] pshListenerArray);
        [DllImport("CGPIB.DLL")]
        static extern int GpibGetAddrInfo(short shDevId, ref short pshTalker, [MarshalAs(UnmanagedType.LPArray)] short[] pshListenerArray);

        //Serial poll
        [DllImport("CGPIB.DLL")]
        static extern int GpibSPoll(short shEqpId, ref short pshStb, ref short pshSrq);
        [DllImport("CGPIB.DLL")]
        static extern int GpibSPollAll(short shDevId, [MarshalAs(UnmanagedType.LPArray)] short[] pshAddrArray, [MarshalAs(UnmanagedType.LPArray)] short[] pshStbArray, [MarshalAs(UnmanagedType.LPArray)] short[] pshSrqArray);

        //Parallel poll
        [DllImport("CGPIB.DLL")]
        static extern int GpibPPoll(short shDevId, ref short pshPpr);
        [DllImport("CGPIB.DLL")]
        static extern int GpibSetPPoll(short shDevId, [MarshalAs(UnmanagedType.LPArray)] short[] pshAddrArray, [MarshalAs(UnmanagedType.LPArray)] short[] pshDataLineArray, [MarshalAs(UnmanagedType.LPArray)] short[] pshPolarityArray);
        [DllImport("CGPIB.DLL")]
        static extern int GpibGetPPoll(short shDevId, [MarshalAs(UnmanagedType.LPArray)] short[] pshAddrArray, [MarshalAs(UnmanagedType.LPArray)] short[] pshDataLineArray, [MarshalAs(UnmanagedType.LPArray)] short[] pshPolarityArray);
        [DllImport("CGPIB.DLL")]
        static extern int GpibResetPPoll(short shDevId, [MarshalAs(UnmanagedType.LPArray)] short[] pshAddrArray);

        //SRQ response
        [DllImport("CGPIB.DLL")]
        static extern int GpibSetPPollResponse(short shDevId, short shResponse);
        [DllImport("CGPIB.DLL")]
        static extern int GpibGetPPollResponse(short shDevId, ref short pshResponse);
        [DllImport("CGPIB.DLL")]
        static extern int GpibSetIst(short shDevId, short shIst);
        [DllImport("CGPIB.DLL")]
        static extern int GpibGetIst(short shDevId, ref short pshIst);
        [DllImport("CGPIB.DLL")]
        static extern int GpibSendSRQ(short shDevId, short shSrqSend, short shStb);
        [DllImport("CGPIB.DLL")]
        static extern int GpibCheckSPoll(short shDevId, ref short pshSPoll, ref short pshStb);

        //Control
        [DllImport("CGPIB.DLL")]
        static extern int GpibSendCommands(short shDevId, [MarshalAs(UnmanagedType.LPArray)] short[] pshCmdArray);
        [DllImport("CGPIB.DLL")]
        static extern int GpibSendTrigger(short shId);
        [DllImport("CGPIB.DLL")]
        static extern int GpibSendDeviceClear(short shId);
        [DllImport("CGPIB.DLL")]
        static extern int GpibChangeLocal(short shId);
        [DllImport("CGPIB.DLL")]
        static extern int GpibSendLocalLockout(short shDevId);

        //Status
        [DllImport("CGPIB.DLL")]
        static extern int GpibSetStatus(short shDevId, short shSelect, int lnData);
        [DllImport("CGPIB.DLL")]
        static extern int GpibGetStatus(short shDevId, short shSelect, ref int plnData);
        [DllImport("CGPIB.DLL")]
        static extern int GpibReadLines(short shDevId, short shSelect, ref short pshLineStatus);
        [DllImport("CGPIB.DLL")]
        static extern int GpibFindListener(short shDevId, short shPrmAddr, short shScdAddr, ref short pshArraySize, [MarshalAs(UnmanagedType.LPArray)] short[] pshAddrArray);
        [DllImport("CGPIB.DLL")]
        static extern int GpibGetSignal(short shDevId, ref uint pdwwParam, ref uint pdwlParam);

        //Event
        [DllImport("CGPIB.DLL")]
        static extern int GpibSetNotifySignal(short shDevId, uint hWnd, uint dwNotifySignalMask);
        [DllImport("CGPIB.DLL")]
        static extern int GpibGetNotifySignal(short shDevId, ref uint phWnd, ref uint pdwNotifySignalMask);
        [DllImport("CGPIB.DLL")]
        static extern int GpibSetNotifyMessage(short shDevId, int lnMessage);
        [DllImport("CGPIB.DLL")]
        static extern int GpibGetNotifyMessage(short shDevId, ref uint plnMessage);

        //コンストラクタ
        public Cgpib()
        {
        }

        public int Init(string pchDeviceName, out short pshDevId)
        {
            pshDevId = 0;
            int ret = GpibInit(pchDeviceName, ref pshDevId);
            return ret;
        }

        public int Exit(short shDevId)
        {
            int ret = GpibExit(shDevId);
            return ret;
        }

        public int GetErrorString(int lnErrorCode, out string pchErrorString)
        {
            pchErrorString = new string('0', 1);
            System.Text.StringBuilder errorstring = new System.Text.StringBuilder(256);
            int ret = GpibGetErrorString(lnErrorCode, errorstring);
            if (ret == 0)
            {
                pchErrorString = errorstring.ToString();
            }
            return ret;
        }

        //Initialization
        public int SetEquipment(short shDevId, out short pshEqpId, short shPrmAddr, short shScdAddr, short shDelim)
        {
            pshEqpId = 0;
            int ret = GpibSetEquipment(shDevId, ref pshEqpId, shPrmAddr, shScdAddr, shDelim);
            return ret;
        }

        public int GetEquipment(short shEqpId, out short pshPrmAddr, out short pshScdAddr, out short pshDelim)
        {
            pshPrmAddr = 0;
            pshScdAddr = 0;
            pshDelim = 0;
            int ret = GpibGetEquipment(shEqpId, ref pshPrmAddr, ref pshScdAddr, ref pshDelim);
            return 0;
        }

        public int SendIFC(short shDevId, short shIfcTime)
        {
            int ret = GpibSendIFC(shDevId, shIfcTime);
            return ret;
        }

        public int ChangeREN(short shDevId, short shEnable)
        {
            int ret = GpibChangeREN(shDevId, shEnable);
            return ret;
        }

        public int EnableRemote(short shId)
        {
            int ret = GpibEnableRemote(shId);
            return ret;
        }

        //Send and Reception
        public int SendData(short shId, ref int plnSendLen, string pchSendBuf)
        {
            int ret = GpibSendData(shId, ref plnSendLen, pchSendBuf);
            return ret;
        }

        public int SendData(short shId, ref int plnSendLen, byte[] pbSendBuf)
        {
            int ret = GpibSendData(shId, ref plnSendLen, pbSendBuf);
            return ret;
        }

        public int RecData(short shId, ref int plnRecLen, out string pchRecBuf)
        {
            pchRecBuf = new string('0', 1);
            System.Text.StringBuilder recbuf = new System.Text.StringBuilder(256);
            int ret = GpibRecData(shId, ref plnRecLen, recbuf);
            if (ret == 0)
            {
                pchRecBuf = recbuf.ToString();
            }
            return ret;
        }

        public int RecData(short shId, ref int plnRecLen, byte[] pbRecBuf)
        {
            int ret = GpibRecData(shId, ref plnRecLen, pbRecBuf);
            return ret;
        }

        public int SetDelim(short shId, short shDelim, short shEoi, short shEos)
        {
            int ret = GpibSetDelim(shId, shDelim, shEoi, shEos);
            return ret;
        }

        public int GetDelim(short shId, out short pshDelim, out short pshEoi, out short pshEos)
        {
            pshDelim = 0;
            pshEoi = 0;
            pshEos = 0;
            int ret = GpibGetDelim(shId, ref pshDelim, ref pshEoi, ref pshEos);
            return ret;
        }

        public int SetTimeOut(short shDevId, int lnTimeOut)
        {
            int ret = GpibSetTimeOut(shDevId, lnTimeOut);
            return ret;
        }

        public int GetTimeOut(short shDevId, out int plnTimeOut)
        {
            plnTimeOut = 0;
            int ret = GpibGetTimeOut(shDevId, ref plnTimeOut);
            return ret;
        }

        public int SetEscape(short shDevId, short shEnable, short shKeyType, int lnKeyCode)
        {
            int ret = GpibSetEscape(shDevId, shEnable, shKeyType, lnKeyCode);
            return ret;
        }

        public int GetEscape(short shDevId, out short pshEnable, short shKeyType, out int plnKeyCode)
        {
            pshEnable = 0;
            plnKeyCode = 0;
            int ret = GpibGetEscape(shDevId, ref pshEnable, shKeyType, ref plnKeyCode);
            return ret;
        }

        public int SetSlowMode(short shDevId, short shSlowTime)
        {
            int ret = GpibSetSlowMode(shDevId, shSlowTime);
            return ret;
        }

        public int GetSlowMode(short shDevId, out short pshSlowTime)
        {
            pshSlowTime = 0;
            int ret = GpibGetSlowMode(shDevId, ref pshSlowTime);
            return ret;
        }

        public int SetSmoothMode(short shDevId, short shMode)
        {
            int ret = GpibSetSmoothMode(shDevId, shMode);
            return ret;
        }

        public int GetSmoothMode(short shDevId, out short pshMode)
        {
            pshMode = 0;
            int ret = GpibGetSmoothMode(shDevId, ref pshMode);
            return ret;
        }

        public int SetAddrInfo(short shDevId, short shTalker, short[] pshListenerArray)
        {
            int ret = GpibSetAddrInfo(shDevId, shTalker, pshListenerArray);
            return ret;
        }

        public int GetAddrInfo(short shDevId, out short pshTalker, ref short[] pshListenerArray)
        {
            pshTalker = 0;
            int ret = GpibGetAddrInfo(shDevId, ref pshTalker, pshListenerArray);
            return ret;
        }

        //Serial poll
        public int SPoll(short shEqpId, out short pshStb, out short pshSrq)
        {
            pshStb = 0;
            pshSrq = 0;
            int ret = GpibSPoll(shEqpId, ref pshStb, ref pshSrq);
            return ret;
        }

        public int SPollAll(short shDevId, ref short[] pshAddrArray, ref short[] pshStbArray, ref short[] pshSrqArray)
        {
            int ret = GpibSPollAll(shDevId, pshAddrArray, pshStbArray, pshSrqArray);
            return ret;
        }

        //Parallel poll
        public int PPoll(short shDevId, out short pshPpr)
        {
            pshPpr = 0;
            int ret = GpibPPoll(shDevId, ref pshPpr);
            return ret;
        }

        public int SetPPoll(short shDevId, short[] pshAddrArray, short[] pshDataLineArray, short[] pshPolarityArray)
        {
            int ret = GpibSetPPoll(shDevId, pshAddrArray, pshDataLineArray, pshPolarityArray);
            return ret;
        }

        public int GetPPoll(short shDevId, ref short[] pshAddrArray, ref short[] pshDataLineArray, ref short[] pshPolarityArray)
        {
            int ret = GpibGetPPoll(shDevId, pshAddrArray, pshDataLineArray, pshPolarityArray);
            return ret;
        }

        public int ResetPPoll(short shDevId, short[] pshAddrArray)
        {
            int ret = GpibResetPPoll(shDevId, pshAddrArray);
            return ret;
        }

        //SRQ response
        public int SetPPollResponse(short shDevId, short shResponse)
        {
            int ret = GpibSetPPollResponse(shDevId, shResponse);
            return ret;
        }

        public int GetPPollResponse(short shDevId, out short pshResponse)
        {
            pshResponse = 0;
            int ret = GpibGetPPollResponse(shDevId, ref pshResponse);
            return ret;
        }

        public int SetIst(short shDevId, short shIst)
        {
            int ret = GpibSetIst(shDevId, shIst);
            return ret;
        }

        public int SendSRQ(short shDevId, short shSrqSend, short shStb)
        {
            int ret = GpibSendSRQ(shDevId, shSrqSend, shStb);
            return ret;
        }

        public int CheckSPoll(short shDevId, out short pshSPoll, out short pshStb)
        {
            pshSPoll = 0;
            pshStb = 0;
            int ret = GpibCheckSPoll(shDevId, ref pshSPoll, ref pshStb);
            return ret;
        }

        //Control
        public int SendCommands(short shDevId, short[] pshCmdArray)
        {
            int ret = GpibSendCommands(shDevId, pshCmdArray);
            return ret;
        }

        public int SendTrigger(short shId)
        {
            int ret = GpibSendTrigger(shId);
            return ret;
        }

        public int SendDeviceClear(short shId)
        {
            int ret = GpibSendDeviceClear(shId);
            return ret;
        }

        public int ChangeLocal(short shId)
        {
            int ret = GpibChangeLocal(shId);
            return ret;
        }

        public int SendLocalLockout(short shDevId)
        {
            int ret = GpibSendLocalLockout(shDevId);
            return ret;
        }

        //Status
        public int SetStatus(short shDevId, short shSelect, int lnData)
        {
            int ret = GpibSetStatus(shDevId, shSelect, lnData);
            return ret;
        }

        public int GetStatus(short shDevId, short shSelect, out int plnData)
        {
            plnData = 0;
            int ret = GpibGetStatus(shDevId, shSelect, ref plnData);
            return ret;
        }

        public int ReadLines(short shDevId, short shSelect, out short pshLineStatus)
        {
            pshLineStatus = 0;
            int ret = GpibReadLines(shDevId, shSelect, ref pshLineStatus);
            return ret;
        }

        public int FindListener(short shDevId, short shPrmAddr, short shScdAddr, ref short pshArraySize, short[] pshAddrArray)
        {
            int ret = GpibFindListener(shDevId, shPrmAddr, shScdAddr, ref pshArraySize, pshAddrArray);
            return ret;
        }

        public int GetSignal(short shDevId, out uint pdwwParam, out uint pdwlParam)
        {
            pdwwParam = 0;
            pdwlParam = 0;
            int ret = GpibGetSignal(shDevId, ref pdwwParam, ref pdwlParam);
            return ret;
        }

        //Event
        public int SetNotifySignal(short shDevId, uint hWnd, uint dwNotifySignalMask)
        {
            int ret = GpibSetNotifySignal(shDevId, hWnd, dwNotifySignalMask);
            return ret;
        }

        public int GetNotifySignal(short shDevId, out uint phWnd, out uint pdwNotifySignalMask)
        {
            phWnd = 0;
            pdwNotifySignalMask = 0;
            int ret = GpibGetNotifySignal(shDevId, ref phWnd, ref pdwNotifySignalMask);
            return ret;
        }

        public int SetNotifyMessage(short shDevId, int lnMessage)
        {
            int ret = GpibSetNotifyMessage(shDevId, lnMessage);
            return ret;
        }

        public int GetNotifyMessage(short shDevId, out uint plnMessage)
        {
            plnMessage = 0;
            int ret = GpibGetNotifyMessage(shDevId, ref plnMessage);
            return ret;
        }
    }
}