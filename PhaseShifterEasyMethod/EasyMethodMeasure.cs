﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Numerics;
using System.Threading;
using System.Windows.Forms;
using System.IO;
using MathNet.Numerics.LinearAlgebra;

using Instruments;
using Utils;
using Gpib;
using Calculation;

namespace PhaseShifterEasyMethod
{
    class EasyMethodMeasure
    {
        private readonly static int stdCount = 3;

        // 機器設定
        private Dictionary<InstType, DeviceSetting> ds;
        private FreqRange fr;
        private double oscPower;
        private int waitTime;
        private int refPortNum;
        private string saveFolder;
        private int sumOfPoints;
        private List<double> delayList;
        private IProgress<PortPowerStatus> progress;
        private CancellationToken ct;

        public EasyMethodMeasure(
            Dictionary<InstType, DeviceSetting> ds, FreqRange fr,
            double oscPower, int waitTime, int refPortNum, string saveFolder, CancellationToken ct)
        {
            this.ds = ds;
            this.oscPower = oscPower;
            this.waitTime = waitTime;
            this.refPortNum = refPortNum;
            this.saveFolder = saveFolder;
            this.fr = fr;
            this.sumOfPoints = fr.Count * (stdCount + 1);
            this.delayList = getDelayList(fr.FreqList);
            this.ct = ct;
        }

        public async Task Measure()
        {
            var ppform = new PowerPlot_Form(fr);
            ppform.Show();
            try
            {
                progress = new Progress<PortPowerStatus>(ppform.AddData);
                using (var dm = new DeviceManager(ds, fr))
                {
                    await Task.Run(() => measureLoop(dm));
                }
                System.Media.SystemSounds.Asterisk.Play();
            }
            catch (OperationCanceledException ex)
            {
                MessageBox.Show(ex.Message, "中断",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            ppform.Close();
        }

        public void measureLoop(DeviceManager dm)
        {
            var stdSparaFiles = new SparaFile[stdCount];
            var stdPowerFiles = new PortPowerFile[stdCount];
            dm.Osc.SetFreq(fr[0]);
            dm.Osc.SetPower(oscPower);
            dm.Osc.SetOutput(true);
            dm.Ts.SetState(SwitchState.CAL);
            for (int n = 0; n < stdCount; n++)
            {
                var std_f = new Complex[fr.Count];
                var power_fh = new double[fr.Count][];
                for (int f = 0; f < fr.Count; f++)
                {
                    ct.ThrowIfCancellationRequested();
                    double delay = dm.Ps.SetDelay(delayList[f] * n / stdCount);
                    dm.Osc.SetFreq(fr[f]);
                    Thread.Sleep(waitTime);
                    power_fh[f] = dm.Pm.GetPower(fr[f], "標準器電力ファイル" + n + "の選択");
                    progress.Report(new PortPowerStatus(
                        fr[f], power_fh[f], 100 * (fr.Count * n + f) / sumOfPoints));
                    std_f[f] = Complex.FromPolarCoordinates(1, Misc.DelayToDegree(delay * 1.0e-12, fr[f]));
                }
                stdSparaFiles[n] = new SparaFile(std_f, fr);
                stdPowerFiles[n] = new PortPowerFile(power_fh, fr, power_fh[0].Length);
                stdPowerFiles[n].Save(Path.Combine(saveFolder, "s" + n + ".csv"));
                stdSparaFiles[n].Save(Path.Combine(saveFolder, "s" + n + ".s1p"));
            }
            dm.Ts.SetState(SwitchState.CalRef);
            var refPower_fh = new double[fr.Count][];
            for (int f = 0; f < fr.Count; f++)
            {
                ct.ThrowIfCancellationRequested();
                dm.Osc.SetFreq(fr[f]);
                Thread.Sleep(waitTime);
                refPower_fh[f] = dm.Pm.GetPower(fr[f], "基準ポート電力（測定波 = 0）ファイルの選択");
                progress.Report(new PortPowerStatus(
                    fr[f], refPower_fh[f], 100 * (fr.Count * stdCount + f) / sumOfPoints));
            }
            var refPowerFile = new PortPowerFile(refPower_fh, fr, refPower_fh[0].Length);
            refPowerFile.Save(Path.Combine(saveFolder, "ref.csv"));
            dm.Osc.SetOutput(false);

            var calc = new SysParaCalculator();
            var sysParaFile = calc.CalculateFromFile(refPowerFile, stdPowerFiles, stdSparaFiles, refPortNum);
            sysParaFile.Save(Path.Combine(saveFolder, "sysp.csv"));
        }

        private static List<double> getDelayList(List<long> fl)
        {
            int midIndex = fl.Count / 2;
            double midDelay = 1.0 / fl[midIndex];

            // 60 deg以上離れていなければ分割
            if (1.0 / 3.0 * midDelay * fl[0] < 60.0 / 360.0
                && 2.0 / 3.0 * midDelay * fl[fl.Count - 1] > 300.0 / 360.0)
            {
                List<double> lower = getDelayList(fl.GetRange(0, midIndex));
                List<double> upper
                    = getDelayList(fl.GetRange(midIndex, fl.Count - midIndex));
                lower.AddRange(upper);
                return lower;
            }
            else
                return Enumerable.Repeat<double>(midDelay * 1.0e12, fl.Count).ToList();
        }
    }
}
