﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Numerics;
using MathNet.Numerics.LinearAlgebra;

namespace Calculation
{
    internal class NewtonOptimizer
    {
        internal static Complex OptimizeCircleClossing(IEnumerable<Circle> circles, Complex position)
        {
            if (circles.Count() < 3)
                throw new Exception("円が3個以下しかありません");

            var previous = Vector<double>.Build.DenseOfArray(new double[] { position.Real, position.Imaginary });
            Vector<double> current;
            Complex result;
            int Count = 0; 
            while (true)
            {
                var div = CalcDivergenseVector(circles, position);
                var hesse = CalcHesseMatrix(circles, position);
                current = previous - hesse.Inverse() * div;
                result = new Complex(current[0], current[1]);

                // 収束判定
                if ((current - previous).L2Norm() < 0.0000000001)
                    break;
                
                current = previous;
                // 収束に失敗
                if(++Count >= 10000)
                    throw new Exception("収束しませんでした");
            }
            return new Complex(current[0], current[2]);
        }

        private static Vector<double> CalcDivergenseVector(IEnumerable<Circle> circles, Complex position)
        {
            var divergense = Vector<double>.Build.Dense(2);
            foreach (var circle in circles)
            {
                Complex diist = position - circle.Center;
                divergense[0] += 2 * (1 - circle.Radius / Complex.Abs(diist)) * diist.Real;
                divergense[1] += 2 * (1 - circle.Radius / Complex.Abs(diist)) * diist.Imaginary;
            }
            return divergense;
        }

        private static Matrix<double> CalcHesseMatrix(IEnumerable<Circle> circles, Complex position)
        {
            var hesseMatrix = Matrix<double>.Build.Dense(2, 2, 0);
            foreach (var circle in circles)
            {
                Complex dist = position - circle.Center;
                hesseMatrix[0, 0] += 2 * (1 + (Math.Pow(dist.Real / Complex.Abs(dist), 2) - 1)
                    * circle.Radius / Complex.Abs(dist));
                hesseMatrix[1, 1] += 2 * (1 + (Math.Pow(dist.Imaginary / Complex.Abs(dist), 2) - 1)
                    * circle.Radius / Complex.Abs(dist));
                double d2f_dxdy = 2 * dist.Real * dist.Imaginary * circle.Radius / Math.Pow(Complex.Abs(dist), 3);
                hesseMatrix[1, 0] += d2f_dxdy;
                hesseMatrix[0, 1] += d2f_dxdy;
            }
            return hesseMatrix;
        }
    }
}
