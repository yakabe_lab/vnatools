﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Instruments
{
    public enum ConnectionType { Gpib, Serial, None }

    public class ConnectInfo
    {
        public ConnectionType Type { get; private set; }
        public int PortNum { get; private set; }
        public string Name { get; private set; }

        public ConnectInfo(ConnectionType type, int port, string name)
        {
            Type = type;
            PortNum = port;
            Name = name;
        }

        public override string ToString()
        {
            string s;
            switch (Type)
            {
                case ConnectionType.Gpib:
                    s = "GPIB:" + PortNum;
                    break;
                case ConnectionType.Serial:
                    s = "COM" + PortNum;
                    break;
                case ConnectionType.None:
                    s = "";
                    break;
                default:
                    throw new Exception("接続タイプがおかしい");
            }
            return s + " " + Name;
        }
    }
}
