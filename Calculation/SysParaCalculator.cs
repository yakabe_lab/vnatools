﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Numerics;
using MathNet.Numerics.LinearAlgebra;
using Utils;

namespace Calculation
{
    public class SysParaCalculator
    {
        public CircleViewInfo[] CircleViewInfos { get; private set; }
        private int stdCount;
        private int portCount;
        private FreqRange freqRange;
        private int refPortNum;

        public SysParaCalculator() { }

        public SysParaFile CalculateFromFile(
            PortPowerFile refPowerFile, PortPowerFile[] stdPowerFiles,
            SparaFile[] sFiles, int refPort, int outputPort = 0, int inputPort = 0)
        {
            portCount = refPowerFile.PortCount;
            freqRange = refPowerFile.FreqRange;
            stdCount = sFiles.Length;
            this.refPortNum = refPort;

            if (stdPowerFiles.Length != stdCount)
                throw new Exception("電力ファイルとSパラメータファイルの数が違います");
            if (stdCount < 3)
                throw new Exception("標準器の数が3以下です");
            if (portCount < 4)
                throw new Exception("ポート数が4以下です");
            if (stdPowerFiles.Any(spf => spf.PortCount != portCount))
                throw new Exception("ポート数が違います");
            if (sFiles.Any(sFile => stdCount > outputPort && stdCount > inputPort))
                throw new Exception("選択したポート番号は標準器のSパラメータファイルに存在しません");

            if (stdPowerFiles.Select(p => p.FreqRange).All(fr => fr == refPowerFile.FreqRange)
                && sFiles.Select(p => p.FreqRange).All(fr => fr == refPowerFile.FreqRange))
                throw new Exception("周波数範囲が違います");

            var SystemParameters = new SysPara[freqRange.Count];
            for (int freqIndex = 0; freqIndex < freqRange.Count; freqIndex++)
            {
                var T_i = refPowerFile[freqIndex].Where((p, h) => h != refPort)
                        .Select(p => p / refPowerFile[freqIndex][refPort]).ToArray();

                var t_h_byRadicalCenter = new Complex[portCount];
                var t_h_byNewtonMethod = new Complex[portCount];
                for (int h = 0; h < portCount; h++)
                {
                    var pBar_n = new double[stdCount];
                    var std_n = new Complex[stdCount];
                    var stdConjugate_n = new Complex[stdCount];
                    var stdAbs2_n = new Complex[stdCount];
                    var circle_n = new Circle[stdCount];
                    for (int n = 0; n < stdCount; n++)
                    {
                        pBar_n[n] = stdPowerFiles[n][freqIndex][h] / refPowerFile[freqIndex][h];
                        std_n[n] = sFiles[n][freqIndex][outputPort, inputPort];
                        stdConjugate_n[n] = Complex.Conjugate(std_n[n]);
                        stdAbs2_n[n] = Complex.Abs(std_n[n]) * Complex.Abs(std_n[n]);
                        circle_n[n] = new Circle(-1 / std_n[n], pBar_n[n] / std_n[n].Magnitude);
                    }

                    // 根心による解は標準器3つあれば十分
                    // 擬似逆行列使ってもいいけどそこまでは実装しない
                    var A = Matrix<Complex>.Build.DenseOfColumns(
                        new IEnumerable<Complex>[] {std_n.Take(3), stdConjugate_n.Take(3), stdAbs2_n.Take(3)});
                    var b = Vector<Complex>.Build.DenseOfEnumerable(pBar_n.Select(p => Complex.One * (p - 1)));
                    t_h_byRadicalCenter[h] = A.Solve(b)[2];
                    t_h_byNewtonMethod[h] = NewtonOptimizer.OptimizeCircleClossing(circle_n, t_h_byRadicalCenter[h]);
                }
            }
            var sysParaFile = new SysParaFile(SystemParameters, freqRange, refPort);
            return sysParaFile;
        }
    }
}
