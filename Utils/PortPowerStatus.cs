﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utils
{
    public class PortPowerStatus
    {
        public long Freq { get; private set; }
        public double[] Power { get; private set; }
        public int Percentage { get; private set; }

        public PortPowerStatus(long freq, double[] power, int parcentage)
        {
            Freq = freq;
            Power = power;
            Percentage = parcentage;
        }
    }
}
