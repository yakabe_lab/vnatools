﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Instruments.Oscillator
{
    public interface IOscillator : IDisposable, IInstrument
    {
        void SetOutput(bool on);
        void SetFreq(long freq);
        void SetPower(double power);
    }
}
