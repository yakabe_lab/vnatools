﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Instruments.PhaseShifter
{
    class Ps_Dummy : IPhaseShifter
    {
        public double Delay { private set; get; } // 相対遅延

        public Ps_Dummy() { }
        public void SetAbsoluteStep(int step) { }
        public void SetRelativeStep(int step) { }
        public void Rotate(int step, Direction d) { }
        public void Dispose() { }

        public double SetDelayDifference(double delay)
        {
            return SetDelay(delay - this.Delay);
        }

        public double SetDelay(double delay)
        {
            Delay = delay;
            return Delay;
        }

    }
}
