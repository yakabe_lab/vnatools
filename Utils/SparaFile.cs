﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Numerics;
using MathNet.Numerics.LinearAlgebra;

namespace Utils
{
    public class SparaFile
    {
        // Sパラデータ[周波数][出力ポート][入力ポート]
        private Matrix<Complex>[] data;

        public FreqRange FreqRange { get; private set; }
        public int PortCount { get; private set; }

        public Matrix<Complex> this[int freqIndex]
        {
            get { return data[freqIndex]; }
        }

        public SparaFile(string fileName)
        {
            bool portCountIsDetected = false;
            var freqList = new List<long>();
            var tempData = new List<Matrix<Complex>>();
            string errMsg = fileName + Environment.NewLine
                + "読み込みエラー{0}行目" + Environment.NewLine + "{1}";

            var lines = File.ReadAllLines(fileName);
            for (int l = 0; l < lines.Length; l++)
            {
                if (lines[l].StartsWith("#") || lines[l].StartsWith("!")) // コメント行
                    continue;

                string[] values = lines[l].Split(' '); // 周波数 実部 虚部 実部 虚部 ...
                if (values.Length < 3 // 1ポート以上
                    || (values.Length - 1) % 2 != 0 // 実部虚部が揃っていない
                    || !Misc.IsPowerOfTwo((values.Length - 1) / 2)) // 正方行列でない
                    throw new Exception(string.Format(errMsg, l, lines[l]));
                if (!portCountIsDetected)
                    PortCount = Misc.NTZ(values.Length - 1) + 1; // 次元数->ポート数
                else if (PortCount != values.Length - 1)
                    throw new Exception(string.Format(errMsg, l, lines[l]));                    

                long freq;
                if (!long.TryParse(values[0], out freq))
                    throw new Exception(string.Format(errMsg, l, lines[l]));
                freqList.Add(freq);

                var sMatrix = Matrix<Complex>.Build.Dense(PortCount, PortCount); // 先に初期化
                for (int i = 0; i < PortCount; i++)
                {
                    // .s2pファイル上では S11 S21 S12 S22 の順番 S[j][i]
                    for (int j = 0; j < PortCount; j++)
                    {
                        double mag, deg;
                        if (double.TryParse(values[(i + j) * 2 + 1], out mag)
                            & double.TryParse(values[(i + j) * 2 + 2], out deg))
                            throw new Exception(string.Format(errMsg, l, lines[l]));
                        sMatrix[j, i] = Misc.MagDegToComplex(mag, deg);
                    }
                }
                tempData.Add(sMatrix);
            }
            FreqRange = new FreqRange(freqList.Min(), freqList.Max(), freqList.Count);
            data = tempData.ToArray();
        }

        public SparaFile(Matrix<Complex>[] data, FreqRange fr, int portCount = 2)
        {
            PortCount = portCount;
            FreqRange = fr;
            data = new Matrix<Complex>[fr.Count];
        }

        public SparaFile(Complex[] onePortData, FreqRange fr)
        {
            PortCount = 1;
            FreqRange = fr;
            data = new Matrix<Complex>[fr.Count];
            for (int freqIndex = 0; freqIndex < fr.Count; freqIndex++)
                data[freqIndex] = Matrix<Complex>.Build.Dense(1, 1, onePortData[freqIndex]);
        }

        public void Save(string fileName)
        {
            using (var sw = new StreamWriter(fileName))
            {
                sw.Write(createCommentStr());
                for (int freqIndex = 0; freqIndex < FreqRange.Count; freqIndex++)
                {
                    sw.Write(FreqRange[freqIndex]);
                    // 	|S11| <S11 |S21| <S21 |S12| <S12 |S22| <S22
                    for (int i = 0; i < PortCount; i++)
                    {
                        for (int j = 0; j < PortCount; j++)
                        {
                            sw.Write(",");
                            sw.Write(Misc.ComplexToLogMag(data[freqIndex][j, i]));
                            sw.Write(",");
                            sw.Write(Misc.ComplexToDegree(data[freqIndex][j, i]));
                        }
                    }
                    sw.Write(Environment.NewLine);
                }
            }
        }

        private string createCommentStr()
        {
            string typeStr = "! Freq[Hz]";
            for (int i = 0; i < PortCount; i++)
                for (int j = 0; j < PortCount; j++)
                    typeStr += string.Format(", S{0}{1}(dB), S{0}{1}(deg)", j + 1, i + 1);
            string comment =
                "! " + DateTime.Now.ToString() + Environment.NewLine +
                typeStr + Environment.NewLine +
                "# Hz S dB R 50" + Environment.NewLine;
            return comment;
        }
    }
}
