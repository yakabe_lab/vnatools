﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Gpib;

namespace Instruments.PowerMeter
{
    public class Pm_Dummy : IPowerMeter
    {
        private static Random random = new Random();

        private static readonly int portCount = 4;

        public int PortCount { get { return portCount; } }

        public Pm_Dummy() { }

        public void Dispose() { }

        public double[] GetPower(long freq, string msg = "")
        {
            return new double[portCount].Select(i => random.NextDouble()).ToArray();
        }
    }
}
